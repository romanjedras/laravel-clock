
jQuery(document).ready(function(){

jQuery(".calcc,.adnotacja").fadeOut(100);
    
jQuery('input#workerField').on('input',function(e){
    
   if(jQuery(this).data("lastval")!= jQuery(this).val()){
       jQuery(this).data("lastval",jQuery(this).val());
       var aa = jQuery(this).val()
       function Round(n, k) 
       {
           var factor = Math.pow(10, k+1);
           n = Math.round(Math.round(n*factor)/10);
           return n/(factor/10);
       }
     
       if(jQuery.isNumeric(aa)){
           if(aa < 25 ){
        jQuery('.res').fadeOut("fast", function(){
           var div = jQuery("<div class='res refbad'>Refinansowanie jest możliwe przy zatrudnieniu powyżej 25 pracowników</div>").hide();
        jQuery('.res').replaceWith(div);
        jQuery('.res').fadeIn("fast");
        jQuery( ".calcus" ).animate({opacity: 0.4}, 200 );
        jQuery(".calcc").fadeOut(200);
            
        });
               
           }else{
            jQuery("#worker_komunikat1").fadeOut(100);
            var result = Round(aa * 0.06 * 1672 / 2, 2);
            jQuery('.res').fadeOut("fast", function(){
var div = jQuery("<div class='res'>"+result+"<span class='gwiazdka'>*</span></div>").hide();
				jQuery('.res').replaceWith(div);
				jQuery('.res').fadeIn("fast");
				jQuery( ".calcus" ).animate({opacity: 1}, 200 ).addClass("pointer");
				jQuery(".calcc,.adnotacja").fadeIn(200);
            });   
           }
           
       }else{
           jQuery("#worker_komunikat1").fadeIn(100);
           jQuery( ".calcus" ).animate({opacity: 0.4}, 200 );
           jQuery(".calcc").fadeOut(100);
       }
       
       
   }
    
});
    
    if ( ($(window).height() + 100) < $(document).height() ) {
    $('#top-link-block').removeClass('hidden').affix({
        // how far to scroll down before link "slides" into view
        offset: {top:100}
    });
}
    
});