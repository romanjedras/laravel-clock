$(function() {
    var facebookboxanimating = false, facebookbox = $('#facebook-likebox');
    $('#facebook-likebox-button').click(function(e) {
        e.preventDefault();

        if (facebookboxanimating) {
            return false;
        }

        facebookboxanimating = true;
        if (facebookbox.css('left') != '0px') {     //jeśli okienko jest schowane, pokazujemy je
            facebookbox.animate({
                left: 0
            }, 500, function() {     //w tym miejscu, w milisekundach podajemy, jak szybko okno ma się pokazywać - moja wartość to 500 ms
                facebookboxanimating = false;
            });
        } else {     //jeśli nie jest, chowamy
            facebookbox.animate({
                left: -305     //wartość identyczna, jak ta w pliku CSS
            }, 500, function() {      //w tym miejscu, w milisekundach podajemy, jak szybko okno ma się chować - moja wartość to 500 ms
                facebookboxanimating = false;
            });
        }

        return true;
    });
});