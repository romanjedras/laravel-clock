$(function() {
    var facebookboxanimating = false, facebookbox = $('#facebook-likebox');
    $('#facebook-likebox-button').click(function(e) {
        e.preventDefault();

        if (facebookboxanimating) {
            return false;
        }

        facebookboxanimating = true;
        if (facebookbox.css('right') != '0px') {     //je�li okienko jest schowane, pokazujemy je
            facebookbox.animate({
                right: 0
            }, 500, function() {     //w tym miejscu, w milisekundach podajemy, jak szybko okno ma si� pokazywa� - moja warto�� to 500 ms
                facebookboxanimating = false;
            });
        } else {     //je�li nie jest, chowamy
            facebookbox.animate({
                right: -305     //warto�� identyczna, jak ta w pliku CSS
            }, 500, function() {      //w tym miejscu, w milisekundach podajemy, jak szybko okno ma si� chowa� - moja warto�� to 500 ms
                facebookboxanimating = false;
            });
        }

        return true;
    });
});