@extends('masterguest')
@section('content')

<style type="text/css">
.floatl.calcf {
    width: 335px;
}
.calc-txt {
    margin: 52px 0 20px;
} 
    
.res.refbad {
    font-size: 16px;
    line-height: 16px;
}
.res {
    padding: 28px 30px;
    font-size: 30px;
    line-height: 30px;
    text-align: center;
}    
    
.float-container.calcu {
    padding: 5% 0 0;
}
.float-container {
    display: table;
    clear: both;
    width: 100%;
}   
form#myForm {
    position: relative;
}
    
.calcf label {
    display: block;
    text-align: center;
    background: #f8b20f;
    padding: 5px 10px;
    font-style: italic;
    position: relative;
}
    
.calcf #workerField {
    display: block;
    padding: 10px;
    width: 100%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    background: transparent;
    border: 2px solid #f8b20f;
    color: #fff;
    text-align: center;
    margin: 5px 0 0;
   font-size: 20px;
}  
    
.calcbg {
    background: url(https://uslugi-korporacyjne.grupaaf.pl/templates/intro/img/bg_picture.jpg) center center no-repeat fixed;
    background-size: cover;
    width: 335px;
    height: 33px;
    position: absolute;
    top: 40px;
    z-index: -1;
    filter: blur(5px);
    -webkit-filter: blur(5px);
    -moz-filter: blur(5px);
    -o-filter: blur(5px);
    -ms-filter: blur(5px);
}  
    
.calci {
    padding-top: 26px;
    position: relative;
}
.calci, .calcf {
    width: 335px;
}


.calci, .calcf {
    width: 335px;
}

.calci #info p {
    font-size: 27px;
    line-height: 32px;
}
span.cs1 {
    font-weight: bold;
    text-transform: uppercase;
}

span.cs2 {
    color: #f8b20f;
    font-weight: bold;
    display: block;
}
.calcr {
    background: url(https://uslugi-korporacyjne.grupaaf.pl/templates/intro/img/vso.png) center center no-repeat;
    width: 300px;
    background-size: 100%;
    height: 100px;
    margin: 0 auto 0 0;
} 
    

#worker_komunikat1.not-success {
    padding: 5px 10px;
    text-align: center;
    font-size: 14px;
    text-transform: uppercase;
    background: #dd0000;
    margin: 5px 0 0;
    color: #FFF;
}
.calcc {
    position: absolute;
    right: 0;
    bottom: 95px;
    color: #333;
    font-size: 30px;
    line-height: 30px;
}
.calcus {
    background: #f8b20f;
    padding: 10px 15px;
    font-size: 22px;
    line-height: 25px;
    letter-spacing: 1.2px;
    text-align: center;
    margin: 15px 0 0;
    cursor: auto;
    opacity: 0.4;
}
.calcus p {
    margin: 0;
}
calcus .cs1 {
    text-transform: lowercase;
}

span.cs1 {
    font-weight: bold;
    text-transform: uppercase;
}
    
.navbar-fixed-top + .content-container {
	margin-top: 70px;
}
.content-container {
	margin: 0 130px;
}



#top-link-block.affix-top {
    position: absolute; /* allows it to "slide" up into view */
    bottom: -82px; /* negative of the offset - height of link element */
    left: 10px; /* padding from the left side of the window */
}
#top-link-block.affix {
    position: fixed; /* keeps it on the bottom once in view */
    bottom: 18px; /* height of link element */
    left: 10px; /* padding from the left side of the window */
}
    
    
</style>



<div class="flex-center position-ref full-height">
<h3>{{$header}}</h3>
        </div>
        
<div class="row">
 <div class="float-container calcu" id="top">
  <div class="calcf col-md-6">
<div class="calc-txt">
		Podaj liczbę pracowników, aby móc dowiedzieć się do jakiej wysokości budżetu zwrócone zostanie do 100% wartości faktury.     
	</div>
	<form id="myForm" class="form-horizontal" method="post">
		<label for="workerField">Podaj liczbę pracowników</label>
		<input type="text" class="form-control" id="workerField" name="workerField" value="" />
		<div class="calcbg"></div>
		<div id="worker_komunikat1" class="not-success" style="display:none;">Podana wartość musi być liczbą</div>
		<?php /*<input type="submit" id="mybutton" class="btn btn-primary" value="Policz">*/?>
	</form>          
      
  </div>  
   <div class="calci col-md-6">
    <div id="info">
<p><span class="cs1">maksymalna</span> miesięczna <span class="cs2">kwota refinansowana<span class="cs3">:</span></span></p>
<div class="calcr"><div class="res">0</div></div>
<div class="calcc">zł</div>
</div>
    <p class="adnotacja">* Niniejsza wartość jest symulacją.<p>
    <div class="calcus"><p><span class="cs1">Zadzwoń</span> i doprecyzuj budżet</div>
     </div>  
    </div>  
   <span id="top-link-block" class="hidden">
    <a href="#top" class="well well-sm"  onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
        <i class="glyphicon glyphicon-chevron-up"></i> Back to Top
    </a>
</span><!-- /top-link-block --> 
</div>    
   
          


						
                 
      
@stop 