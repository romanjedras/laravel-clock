@extends('masterguest')
@section('content')

<div class="flex-center position-ref full-height">
<h3>{{$header}}</h3>
        
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
        
    {!! Form::open(['url'=>'contact','id'=>'create_category','method'=>'post','classes'=>'form-horizontal']) !!}   
    @include('pages.form_errors')        
     <div class="clearfix" style="padding-bottom:6px;"></div>
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Podaj swój nick:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Podaj swój nick')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div>
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('email', 'Podaj swój email:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::email('email',null,array('id'=>'email','required','class'=>'form-control','title'=>'Podaj swój email')) !!}
    </div>
    </div>  
    
 <div class="clearfix" style="padding-bottom:6px;"></div>
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('description', 'Podaj swoja wiadomość:'); !!}
    </div>                                                                                              <div class="col-md-6">     
 {!! Form::textarea('description',null,array('id'=>'description','class'=>'form-control','name'=>'description')) !!}
   </div>
    </div>                      
    
<div class="clearfix" style="padding-bottom:6px;"></div>
     <div class="form-group">
    <div class="col-md-4 control-label"> {!!  Form::label('g-recaptcha', 'Captcha:'); !!}
    </div>
<div class="col-md-6">  
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="g-recaptcha" data-sitekey="{!! env('RECAPTCHA_SITE_KEY','NO-KEY-FOUND') !!}"></div>
   </div>
    </div>
    <div class="clearfix" style="padding-bottom:25px;"></div>
     <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
   
   
        
    </div>
    </div> 
    {!! Form::close() !!}   
       
        </div>
    </div>
    </div>        
        
        
        
        
        </div>
        
        
        
         
        
@stop