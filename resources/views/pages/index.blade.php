@extends('masterguest')
@section('content')   
<div class="flex-container">
	<div class="float-container">
		<h2>
			<span class="sp1">Aby zobaczyć</span>
			<span class="sp2">swoją prezentację</span>
		</h2>
		<div class="btn-container">
			<a href="/login" target="_self" title="Zaloguj się do demo">Zaloguj się do demo</a>
		</div>
	</div>    
</div>    
@stop 