@extends('layouts.master')
@section('content')

<style type="text/css">
.form-group {
    margin-bottom: 0px;
}
</style>



<h2> Edytuj {{$clock->demo}} zegar</h2>

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
             
    {!! Form::model($clock, ['method'=>'PATCH','class'=>'form-horizontal','action'=>['ClocksController@update', $clock->id]]) !!}             
    {!! Form::hidden('user_id',$user_id) !!}
      
 <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('godzina', 'Godzina zegara:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('godzina',null,array('id'=>'godzina','required','class'=>'form-control','title'=>'Wybierz godzinę')) !!}
    </div>
    </div>
 <div class="clearfix" style="padding-bottom:6px;"></div>     
  <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('dzien', 'Dzien miesiaca:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('dzien',null,array('id'=>'dzien','required','class'=>'form-control','title'=>'Wybierz dzień')) !!}
    </div>
    </div>
 <div class="clearfix" style="padding-bottom:6px;"></div>    
   <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('miesiac', 'Miesiac:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('miesiac',\Carbon\Carbon::now('Europe/Warsaw')->month,array('id'=>'miesiac','required','class'=>'form-control','title'=>'Wybierz miesiac')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div>
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('rok', 'Rok:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('rok',null,array('id'=>'rok','required','class'=>'form-control','title'=>'Wybierz rok')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div>
    <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('Wybierz nazwę demo', 'Demo:'); !!}
    </div>
         <div class="col-md-6">
        {!! Form::select('demo',$demos, null ,['class'=>'form-control','title'=>'Wybierz demo']) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:25px;"></div>            
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div>           
   {!! Form::close() !!}
            </div>
        </div>    
      </div>
</div> 
@stop