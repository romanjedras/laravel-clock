@extends('layouts.master')
@section('content')

<style type="text/css">

    .table tbody > tr{
     font-size:12px;
    white-space: nowrap;
    margin:0 auto;    
    }
    
    h4.title {
    font-size: 12px;
    margin: 0 auto;
    text-align: center;
}
    
    
</style>


 <h2>Lista wszytkich ustawionych zegarów wraz z wyznaczonym demo</h2>

    @if (Session::has('clock_created'))
    <div class="alert alert-success card">
        {{Session::get('clock_created')}}
    </div>
    @endif

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
    
                <br/>
    <table class="table table-bordered table-striped">
       <thead>
        <tr>
           <th>Lp.</th>
            <th>Nazwa demo</th>
            <th>Godzina</th>
            <th>Dzien</th>
            <th>Miesiac</th>
            <th>Rok</th>
            <th>Użytkownik</th>
       <th>Edytuj czas demo</th>
        </tr>
        </thead>
        <tbody>
    <tr>
        <div style="margin-bottom:20px">
        <a class="btn btn-success btn-sm" href="{{ action('DemosController@index')}}" >Wróć do wszytkich demo </a> 
        <a class="btn btn-danger btn-sm" href="clocks/create">Dodaj nowy zegar </a> 
        </div>
          
            
    </tr>
       <?php $i=1;?>
            
    @foreach($cloks as $clok) 
      <tr>
      <th scope="row"><?php echo $i;?></th>    
        <td>
           <h4 class="title">{{$clok->demo}}</h4>    
        </td>    
        <td>
        <p class="col-md-3"> {{$clok->godzina}}</p>
        </td>        
         <td>
        <p class="col-md-3"> {{$clok->dzien}}</p>
        </td>     
         <td>
        <p class="col-md-3"> {{$clok->miesiac}}</p>
        </td>     
         <td>
        <p class="col-md-3"> {{$clok->rok}}</p>
        </td>
        @foreach($users as $user)
         @if($clok->user_id == $user->id)
        <td>
        <p class="col-md-3"> {{$user->name}}</p>
        </td>  
        @endif                
           @endforeach        
            
         <td>
         <a href="{{ action('ClocksController@edit', $clok->id) }}" class="btn btn-success btn-sm">
            Edytuj czas
            </a></td>
        <?php $i++;?>
          </tr>
    @endforeach               
            
            
        
       
        </tbody>
    </table>        
                    
                                    
    
            </div>
        </div>    
      </div>
</div>  
@stop      