@extends('layouts.master')
@section('content')




<h2> Dodaj czas zegara</h2>
  
  
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">    
     <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>   
          </div>
        </div>
  </div>
  
  
  

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
            
               @if(count($errors) > 0)
           <div class="allert alert-danger">
               <ul>
               @foreach($errors->all() as $error) 
                   <li>Pole jest wymagane</li>
                @endforeach   
               </ul>
                </div>
        @endif  
            
       {!! Form::open(['url'=>'store_clock','id'=>'create_clock','method'=>'post','classes'=>'form-horizontal']) !!}
       
     {!! Form::hidden('user_id',$user_id) !!}
 <div class="clearfix" style="padding-bottom:6px;"></div>
 <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('godzina', 'Godzina zegara:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('godzina',\Carbon\Carbon::now('Europe/Warsaw')->hour,array('id'=>'godzina','required','class'=>'form-control','title'=>'Wybierz godzinę')) !!}
    </div>
    </div>
 <div class="clearfix" style="padding-bottom:6px;"></div>
 <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('dzien', 'Dzien miesiaca:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('dzien',\Carbon\Carbon::now('Europe/Warsaw')->day,array('id'=>'dzien','required','class'=>'form-control','title'=>'Wybierz dzień')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div>
 <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('miesiac', 'Miesiac:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('miesiac',\Carbon\Carbon::now('Europe/Warsaw')->month,array('id'=>'miesiac','required','class'=>'form-control','title'=>'Wybierz miesiac')) !!}
    </div>
    </div>
     <br/><br/>
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('rok', 'Rok:'); !!}
    </div>
         <div class="col-md-6">
    <span class="form-control" title="Wybierz rok ustawienia zegara">
    {{ Form::selectYear('rok', 2016, 2020,['class'=>'form-control','id' => 'rok','required']) }}    
    </span>
    </div>
    </div>
     <div class="clearfix" style="padding-bottom:6px;"></div>          
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('demo', 'Demo:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::select('demo', $demos, 'demo1',['class'=>'form-control','title'=>'Wybierz demo']) !!}
    </div>
    </div>            
 <div class="clearfix" style="padding-bottom:25px;"></div>
     <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div>           
   
    
{!! Form::close() !!}
            </div>
        </div>    
      </div>
</div>  
@stop      