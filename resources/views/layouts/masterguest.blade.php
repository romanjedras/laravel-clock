<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DEMO-CMS</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700&subset=latin,latin-ext" rel='stylesheet' type='text/css'>
	<style>
		.lbox{position:relative;transition:all .2s ease;position:fixed;width:100%;height:100%;background:#000;z-index:999999999;}
		.loader:before{content:"";position:absolute;top:0px;left:-25px;height:12px;width:12px;border-radius:12px;-webkit-animation:loader10g 3s ease-in-out infinite;animation:loader10g 3s ease-in-out infinite;}
		.loader{position:fixed;z-index:9999999999;width:12px;height:12px;left:50%;top:50%;margin-top:-6px;margin-left:-6px;border-radius:12px;-webkit-animation:loader10m 3s ease-in-out infinite;animation:loader10m 3s ease-in-out infinite;}
		.loader:after{content:"";position:absolute;top:0px;left:25px;height:10px;width:10px;border-radius:10px;-webkit-animation:loader10d 3s ease-in-out infinite;animation:loader10d 3s ease-in-out infinite;}
		@-webkit-keyframes loader10g{0%{background-color:rgba(255,255,255,.2);}25%{background-color:rgba(255,255,255,1);}50%{background-color:rgba(255,255,255,.2);}75%{background-color:rgba(255,255,255,.2);}100%{background-color:rgba(255,255,255,.2);}}
		@keyframes loader10g{0%{background-color:rgba(255,255,255,.2);}25%{background-color:rgba(255,255,255,1);}50%{background-color:rgba(255,255,255,.2);}75%{background-color:rgba(255,255,255,.2);}100%{background-color:rgba(255,255,255,.2);}}
		@-webkit-keyframes loader10m{0%{background-color:rgba(255,255,255,.2);}25%{background-color:rgba(255,255,255,.2);}50%{background-color:rgba(255,255,255,1);}75%{background-color:rgba(255,255,255,.2);}100%{background-color:rgba(255,255,255,.2);}}
		@keyframes loader10m{0%{background-color:rgba(255,255,255,.2);}25%{background-color:rgba(255,255,255,.2);}50%{background-color:rgba(255,255,255,1);}75%{background-color:rgba(255,255,255,.2);}100%{background-color:rgba(255,255,255,.2);}}
		@-webkit-keyframes loader10d{0%{background-color:rgba(255,255,255,.2);}25%{background-color:rgba(255,255,255,.2);}50%{background-color:rgba(255,255,255,.2);}75%{background-color:rgba(255,255,255,1);}100%{background-color:rgba(255,255,255,.2);}}
		@keyframes loader10d{0%{background-color:rgba(255,255,255,.2);}25%{background-color:rgba(255,255,255,.2);}50%{background-color:rgba(255,255,255,.2);}75%{background-color:rgba(255,255,255,1);}100%{background-color:rgba(255,255,255,.2);}}
	</style>
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
     <link href="{{URL::asset('css/normalize.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/custom2.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/jquery.fileupload.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/prism.css')}}" rel="stylesheet">
   <link href="{{URL::asset('css/tooltip.css')}}" rel="stylesheet">
<!--   <link href="{{URL::asset('css/prism.css')}}" rel="stylesheet">-->
   <link href="{{URL::asset('css/tooltip.dream.css')}}" rel="stylesheet">   
    
</head>
<body id="app-layout">
	<div class="lbox"><div class="loader"></div></div>
	<div class="glass"></div>
	<div class="top-header d-view">
		<div class="container">
			<div class="float-container">
				<div class="floatl logo">
					<a title="DEMO-CMS" href="/">
						<h1 class="hide">Grupa AF</h1>
						<img src="{{URL::asset('images/logo.png')}}" alt="GrupaAf">
					</a>
				</div>
				<div class="floatr main-menu">
					<ul class="float-container menu-nav">
						<li><a href="/">Start</a></li>
						<li><a href="https://grupaaf.pl/#map" target="_blank">Kontakt</a></li>
						<li><a href="https://grupaaf.pl/#about" target="_blank">O nas</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div class="zatr-menu-opener m-view">
		<div class="zatr-menu-opener-inner"></div>
	</div>
	<div class="m-menu zatr-menu m-view">
		<div class="zatr-menu-inner"> 
			<ul class="menu-nav">
				<li><a href="/">Start</a></li>
				<li><a href="https://grupaaf.pl/#map" target="_blank">Kontakt</a></li>
				<li><a href="https://grupaaf.pl/#about" target="_blank">O nas</a></li>
			</ul>
		</div>
	</div>
	<div class="m-view m-logo float-container">
		<div class="container">
			<a title="DEMO-CMS" href="/">
				<img src="{{URL::asset('images/logo.png')}}" alt="GrupaAf">
			</a>
		</div>
	</div>
    <!-- wrapper -->
    <div class="site-wrappper">         
    
       
        <!-- .container -->
        <div class="container site-content">
           
           @yield('content')
            
        </div><!-- end of .container -->
        
    </div><!-- end of wrapper -->
    

    <!-- Footer -->
    <footer class="site-footer navbar-fixed-bottom">
        <div class="container">
			<div class="float-container">
				<div class="floatl">
					<h3>
						<span class="sp2">Globalne możliwości</span>
						<span class="sp1">do 100% możliwości</span>
					</h3>
					<div class="inline-elements-block">
						<div class="inline-element"><a href="https://grupaaf.pl/#news" target="_blank" title="Korporacyjna WWW">Korporacyjna WWW</a></div>
						<div class="inline-element sep-item"><a href="https://grupaaf.pl/#news" target="_blank" title="Outsourcing Działu Marketingowego">Outsourcing Działu Marketingowego</a></div>
						<div class="inline-element"><a href="https://grupaaf.pl/#news" target="_blank" title="Wsparcie CSR">Wsparcie CSR</a></div>
						<div class="inline-element sep-item"><a href="https://grupaaf.pl/#news" target="_blank" title="Druk i identyfikacja wizualna">Druk i identyfikacja wizualna</a></div>
					</div>
				</div>
				<div class="floatr">
					<h3>
						<span class="sp2">Usługi dodatkowe</span>
						<span class="sp1">Wsparcie procesów biznesowych</span>
					</h3>
					<div class="inline-elements-block">
						<div class="inline-element"><a href="https://grupaaf.pl/#news" target="_blank" title="Specjalistyczne tłumaczenia">Specjalistyczne tłumaczenia</a></div>
						<div class="inline-element sep-item"><a href="https://grupaaf.pl/#news" target="_blank" title="Copywriting">Copywriting</a></div>
						<div class="inline-element sep-item"><a href="https://grupaaf.pl/#news" target="_blank" title="Audyty">Audyty</a></div>
						<div class="inline-element"><a href="https://grupaaf.pl/#news" target="_blank" title="Prawo autorskie i prasowe">Prawo autorskie i prasowe</a></div>
						<div class="inline-element sep-item"><a href="https://grupaaf.pl/#news" target="_blank" title="Prawo handlowe">Prawo handlowe</a></div>
					</div>
				</div>
			</div>
        </div>
    </footer>


    <!-- JavaScripts -->
	<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script>
		(function($) {
			$(window).load(function() {
				$('.loader').fadeOut();
				$('.lbox').delay(350).fadeOut('slow');
				$('body').delay(350).css({'overflow':'visible'});
			});
		})(jQuery);
		jQuery(".btn-container a").click(function(e){
			e.preventDefault();
			jQuery('#app-layout').html('<img class="loadimg" src="{{URL::asset('images/loading.png')}}" />');
			jQuery("body").attr("id","front-guest");
			jQuery.ajax({
				url: '/login',
				type: "GET",
				success:function(data){jQuery('#front-guest').html(data);}
			});
		});
		jQuery(".zatr-menu-opener,.m-menu a,.glass").click(function(){
			jQuery(".zatr-menu-opener,.zatr-menu-opener-inner,.zatr-menu,.glass").toggleClass("active");
		});
	</script>
</body>
</html>
