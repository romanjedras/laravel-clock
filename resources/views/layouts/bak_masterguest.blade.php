<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DEMO-CMS</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700&subset=latin,latin-ext" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
     <link href="{{URL::asset('css/normalize.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/custom2.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/jquery.fileupload.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/prism.css')}}" rel="stylesheet">
   <link href="{{URL::asset('css/tooltip.css')}}" rel="stylesheet">
<!--   <link href="{{URL::asset('css/prism.css')}}" rel="stylesheet">-->
   <link href="{{URL::asset('css/tooltip.dream.css')}}" rel="stylesheet">
 
  
  
    <style>
        body {
            font-family: 'Lato';
        }
        
        

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    
    
</head>
<body id="app-layout">

 <nav class="navbar navbar-default navbar-fixed-top">
       
        <div class="container">
           <div class="row">
            <div class="navbar-header col-md-2">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#">
                  <h1 class="hide">Grupa AF</h1>
                   <img src="{{URL::asset('images/logo.png')}}" alt="GrupaAf">
                </a>
            </div>

            <div class="collapse navbar-collapse col-md-10" id="app-navbar-collapse">
                
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="http://laravel-demo.grupaaf.pl/">Start</a></li>
                    <li><a href="http://laravel-demo.grupaaf.pl/contact">Kontakt</a></li>
                    <li><a href="http://laravel-demo.grupaaf.pl/about">O nas</a></li>
                </ul>
                
               

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                <li><a href="/login">Login </a></li>    
                </ul>
            </div>
        </div>
     </div>
    </nav>
    <!-- wrapper -->
    <div class="site-wrappper">
<div id="facebook-likebox">
<a id="facebook-likebox-button" href="#"><img src="{{URL::asset('images/slide-button.png')}}" alt="GrupaAf"></a>
<div class="fb-page" data-href="https://www.facebook.com/Agencja-e-kreacji-Grupa-AF-315771558581298/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Agencja-e-kreacji-Grupa-AF-315771558581298/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Agencja-e-kreacji-Grupa-AF-315771558581298/">Agencja e-kreacji Grupa AF</a></blockquote></div>
</div>          
    
       
        <!-- .container -->
        <div class="container site-content">
           
           @yield('content')
            
        </div><!-- end of .container -->
        
    </div><!-- end of wrapper -->
    

    <!-- Footer -->
    <footer class="site-footer navbar-fixed-bottom">
        <div class="container">
            <p>&copy; Grupa AF  2016</p>
        </div>
    </footer>


    <!-- JavaScripts -->
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript" src="{!! asset('js/facebook/tools1.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/calc/calc1.js') !!}"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8&appId=663602263737732";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

		
   
</body>
</html>
