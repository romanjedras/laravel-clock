<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CMS - Demo</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700&subset=latin,latin-ext" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
     <link href="{{URL::asset('css/normalize.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/custom.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/jquery.fileupload.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/prism.css')}}" rel="stylesheet">
   <link href="{{URL::asset('css/tooltip.css')}}" rel="stylesheet">
<!--   <link href="{{URL::asset('css/prism.css')}}" rel="stylesheet">-->
   <link href="{{URL::asset('css/tooltip.dream.css')}}" rel="stylesheet">
  
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body id="app-layout">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="#">
                    CMS - Demo wizualizacja
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="/demos">Demo</a></li>
                    <li><a href="customers">Klienci</a></li>
                    <li><a href="categories">Kategorie</a></li>
                     @if(Auth::user()->canEdit())   
                     <li><a href="adviser">Doradca</a></li>
                    @endif
                    <li><a href="visual">Front Klienta</a></li>
<!--                     <li><a href="/laravel_project/public/logout">Logout </a></li>-->
                </ul>
                
               

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                <li><a href="home">Powrót do Ctr. Panel </a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- wrapper -->
    <div class="site-wrappper">

        <!-- .container -->
        <div class="container site-content">
           
           @yield('content')
            
        </div><!-- end of .container -->
        
    </div><!-- end of wrapper -->
    

    <!-- Footer -->
    <footer class="site-footer navbar-fixed-bottom">
        <div class="container">
            <p>&copy; Grupa AF  <?=date('Y')?></p>
        </div>
    </footer>


    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   <script type="text/javascript" src="{!! asset('js/tooltip/jquery-1.8.0.min.js') !!}"></script>
 <script type="text/javascript" src="{!! asset('js/tooltip/prism.js') !!}"></script>
 <script type="text/javascript" src="{!! asset('js/tooltip/jquery.toc.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/tooltip/modernizr-2.6.1.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/src/jquery-asTooltip.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/clock/jqClock.min.js') !!}"></script>
</body>
</html>
