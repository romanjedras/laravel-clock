<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Wizualizacja Demo </title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{URL::asset('css/normalize.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/zegar/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/custom3.css')}}" rel="stylesheet">
    <link href="{{URL::asset('js/zegar/jquery.countdown.css')}}" rel="stylesheet">
 <link href="{{URL::asset('css/zegar/jquery-bxslider.css')}}" rel="stylesheet">
    <style>
        .fa-btn {
            margin-right: 6px;
        }
    </style>
  <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    </head>
    
   <body id="app-layout" onLoad="javascript:document.body.focus();" onkeydown="return showKeyCode(event)">
       
     
    
     
       <!-- wrapper -->
    <div class="site-wrappper">

        <!-- .container -->
        <div class="container-fluid site-content">
          
          
           @yield('content')
            
        </div><!-- end of .container -->
        
    </div><!-- end of wrapper -->
    

    <!-- Footer -->
    <footer class="site-footer navbar-fixed-bottom">
        <div class="container">
           	<!-- copyright -->
			<div class="copyright">
				<div class="container">
					<div id="author-container"> 
                        <div class="width"> 
                        <div id="author"> 
                        <p><a href="http://grupaaf.pl" target="_blank" rel="copyright" title="projektowanie stron www">Projektowanie stron WWW</a> Grupa AF</p> 
                        </div> 
                        </div> 
                        </div> 
				</div>
			</div>
        </div>
    </footer>
        <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{!! asset('js/zegar/jquery.plugin.js') !!}"></script>   
    <script type="text/javascript" src="{!! asset('js/zegar/jquery.countdown.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/zegar/jquery.countdown-pl.js') !!}"></script>   
    <script type="text/javascript" src="{!! asset('js/zegar/js/jquery.watch.js ') !!}"></script>    
     <!-- Ladowanie obrazkow -->
    <script type="text/javascript" src="{!! asset('js/zegar/js/imagesloaded.pkgd.min.js') !!}"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.js"></script> 
<script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.counterup/1.0/jquery.counterup.min.js"></script>
   </body>
</html>