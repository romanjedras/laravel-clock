<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CMS - demo') }}</title>

     <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700&subset=latin,latin-ext" rel='stylesheet' type='text/css'>
   
   
    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{URL::asset('css/app.css')}}" rel="stylesheet">
    

    <!-- Scripts -->
    
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    
    <script type="text/javascript"> 
    
   
    
    
    </script>
    

    
    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
    
</head>
    
@if (Auth::guest())
<body id="front-guest">                  
@else    
<body id="user">
@endif    

    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    
                    @if (Auth::guest())
                  <div class="row top7">
                    <img src="{{URL::asset('images/login.png')}}" alt="GrupaAF" title="GrupaAF">
                    </div>
                    
                @else
                     <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'CMS DEMO') }}
                    </a>
                    @endif
                </div>
                
               
               <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    
                    
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                  
                        @else
                         <ul class="nav navbar-nav navbar-left"> 
                          <li><a href="/demos">Demo</a></li>
                         <li><a href="/customers">Klienci</a></li>
                           </ul>
                         
                          <!-- Right Side Of Navbar -->
                           <ul class="nav navbar-nav navbar-right">
                           <li><a href="{{ url('/register') }}">Register</a></li>
                           <li><a href="{{ url('/home') }}">Ctr. Panel</a></li>
                          <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                   
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                   </ul>
                </div>
                </div>
            </nav>

        @yield('content')
    </div>

   
        <!-- JavaScripts -->
<script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>

</body>
</html>
