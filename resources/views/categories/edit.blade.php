
@extends('layouts.master')
@section('content')

<style type="text/css">
.form-group {
    margin-bottom: 0px;
}
</style>



<h2> Edytuj nazwę kategorii: {{$category->name}} </h2>

 <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">   
      <a class="btn btn-danger btn-sm" href="{{ action('CategoriesController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do kategorii </a>               
     <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>  
      
          </div>
        </div>
  </div>
</div>



<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">
{!! Form::model($category, ['method'=>'PATCH','class'=>'form-horizontal','action'=>['CategoriesController@update', $category->id]]) !!} 
{!! Form::hidden('user_id',$user_id) !!}
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Podaj nazwę kategorii:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Wybierz nazwę kategorii')) !!}
    </div>
    </div>       
            
<div class="clearfix" style="padding-bottom:25px;"></div>            
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div>           
   {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>




@stop