@extends('layouts.master')
@section('content')



<h2> Dodaj nowa kategorię</h2>


  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">    
     <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>   
          </div>
        </div>
  </div>

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
            
               @if(count($errors) > 0)
           <div class="allert alert-danger">
               <ul>
               @foreach($errors->all() as $error) 
                   <li>Pole jest wymagane</li>
                @endforeach   
               </ul>
                </div>
        @endif
        
        
{!! Form::open(['url'=>'store_category','id'=>'create_category','method'=>'post','classes'=>'form-horizontal']) !!}


 <div class="clearfix" style="padding-bottom:6px;"></div>
 <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Dodaj nowa kategorie:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Dodaj nowa kategorię')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:25px;"></div>
     <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div>           
{!! Form::close() !!}
            </div>
        </div>    
      </div>
</div>     
@stop