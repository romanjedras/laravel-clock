@extends('layouts.master')
@section('content')



<h2>Lista wszytkich ustawionych  kategorii</h2>


<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
                <div class="table-responsive">
        <table class="table table-bordered table-striped">
       <thead>
        <tr>
        <th>Lp.</th>
            <th>Nazwa demo</th>  
            @if(Auth::user()->canEdit())
            <th>Edytuj nazwę kategorii</th>
            @endif
            </tr>
            </thead>
        <tbody>
    <tr>
        <div style="margin-bottom:20px">
        <a class="btn btn-success btn-sm" href="{{ action('DemosController@index')}}" >Wróć do wszytkich demo </a> 
        <a class="btn btn-danger btn-sm" href="categories/create">Dodaj nowa kategorię </a> 
        </div>
          </tr>
           <?php $i=1;?>
           @foreach($categories as $category)
           <tr>
            <th scope="row"><?php echo $i;?></th>
             <td>
           {{$category->name}} 
            </td> 
            @if(Auth::user()->canEdit())
         <td>
        <a href="{{ action('CategoriesController@edit',$category->id)}}" class="btn btn-success btn-sm">
            Edytuj kategorię
            </a></td>
        @endif   
            <?php $i++;?>   
           </tr>
           @endforeach
           
            </tbody> 
        </table>              
                </div>
            </div>
        </div>
    </div>
</div>












@stop