@extends('layouts.masterclock')
@section('content')

<style type="text/css">
    #lista{
        display: none;
        margin-top: 0px;
    }
    
</style>

<style type="text/css" media="print">
    body { visibility: hidden; display: none }
</style> 
<?php 
$sciezka = "filemanager/userfiles/demo/";
$sciezka.= "{$dname}/";
$licznikPlikow = foldery($sciezka);
$files=array();
$files = foldery2($sciezka);
$iloscslajdow = $licznikPlikow;

$liczbaPlikow=array();
     $zmienna =0;
          while($zmienna <= $licznikPlikow) // warunek kontynuacji pętli
            {
          $liczbaPlikow[$zmienna] =$files[$zmienna];
              $zmienna++;
            } 

?>



   

<h2 style="text-align:center;">Jesteś zalogowany jako <span class="customer">{{Auth::user()->name}}</span></h2>

 <ul class="logout-menu" role="menu">
        <li>
            <a href="{{ url('/logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Wyloguj
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
    

<section id="demo">
    <h1 class="hidden">Demo wizualizacja</h1>
 <div id="lista1" style="display:none"></div>
   <div id="toolbarbg"> 
    <div id="toolbar">
        <section class="our-services">
        <h2 class="hidden">Nasze usługi</h2>
        <div class="container">
           <div class="row">
               
    <div class="single-col" data-animate="fadeInLeft">
		<span class="textinfo col-md-6" >Wizualizację przygotowała:</span>
        <a class="col-md-6" target="_blank" href="http://grupaaf.pl"><img src="{{URL::asset('images/logoaf.jpg')}}" alt="GrupaAf"></a>
		
	</div>
	<div class="single-col pracownicy liczba">
<img src="{{URL::asset('img-catalog/iko1.png')}}" alt="Ilość współpracowników" title="Ilość współpracowników">
        <h4 class="hidden">Ile osób na pokładzie</h4>
		<span class="p-liczba"><span class="counter">25</span></span>
		<p class="licztxt">osób na pokładzie</p>
	</div>
	<div class="single-col hosting liczba">
<img src="{{URL::asset('img-catalog/hosting.png')}}" alt="Hostingowani Klienci" title="Hostingowani Klienci">
        <h4 class="hidden">Ile >hostingowanych Klientów</h4>
		<span class="p-liczba"><span class="counter">108</span></span>
		<p class="licztxt1">hostingowanych Klientów</p>
	</div>
	<div class="single-col strony">
<img src="{{URL::asset('img-catalog/iko3.png')}}" alt="Nowi Klienci" title="Nowi Klienci">
        <h4 class="hidden">Ile nowych klientów BIZ/rok</h4>
		<span class="p-liczba"><span class="counter">30</span></span>
		<p class="licztxt1 liczba">nowych klientów BIZ/rok</p>
	</div>           
    <div class="single-col" data-animate="fadeInRight">
    <span><i class="fa fa-comments-o"></i></span>
    <h4 class="hidden">Ile akcji marketingowych m/c</h4>    
        <b><span class="counter">10</span></b>
        <p class="licztxt1 liczba2">akcji marketingowych m/c</p>
				</div>           
       <!-- Sekcja zegar -->
    <div class="single-col" data-animate="fadeInRight">
        <div id="clock">
    <p class="prime">pozostały czas podglądu wizualizacji</p>
   <span id="dni"></span>
      <div id="czas">      
            
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="{!! asset('js/zegar/countdown.js') !!}"></script> 
          
    
<span id="rok" class="hidden" data-field-id="{{$times->rok}}"></span>
<span id="miesiac" class="hidden" data-field-id="{{$times->miesiac}}"></span>
<span id="dzien" class="hidden" data-field-id="{{$times->dzien}}"></span> 
<span id="godzina" class="hidden" data-field-id="{{$times->godzina}}"></span>        
<script>
// Asumning you are using JQuery

var rok = jQuery('#rok').data("field-id");
var miesiac = jQuery('#miesiac').data("field-id");   
var dzien = jQuery('#dzien').data("field-id");    
var godzina = jQuery('#godzina').data("field-id"); 


    $(function () {
		var koncowy_dzien = dzien;
        var ts = new Date();
        ts = new Date(ts.getFullYear(), ts.getMonth(), ts.getUTCDate()+1);
        if(dzien > ts.getUTCDate()){
            var zmienna = dzien - ts.getUTCDate();
				   // jQuery('#div2').show();
				  }else{
					 var zmienna = 0; 
				//jQuery('#div2').hide();
            jQuery('#wizualizacja img').css('-webkit-filter','blur(0px) brightness(1)');
            jQuery('#wizualizacja img').css('-moz-filter','blur(0px) brightness(1)');
				 jQuery('#wizualizacja img').css('-o-filter','blur(0px) brightness(1)');
				 jQuery('#wizualizacja img').css('-ms-filter','blur(0px) brightness(1)');
				 jQuery('#wizualizacja img').css('filter','blur(0px) brightness(1)');
				 }
						if(zmienna == 1){
					jQuery('#dni').html(' dzień');
					   }else{
					jQuery('#dni').html( ' dni');
					   }
				});
    
      var myCountdownTest = new Countdown({
		  //  time: 86400 * 3, // 86400 seconds = 1 day
			labelText :  {
		 // second  : "SEKUND",
		//  minute  : "MINUT",
		//  hour : "GODZIN",
		//  day  : "DNI",
	   //   month  : "MIESIĿCY",
	   //   year  : "LAT"   // <- no comma on last item!
		 },
		  
		 numbers  :  {
		  font  : "Roboto", // Arial Times Verdana etc... see "numberMarginTop" above to correct vertical centering
		  color : "#FFFFFF",
		  bkgd : "none",
		  rounded : 0,    // percentage of size 
		  shadow : {
			 x : 0,   // x offset (in pixels)
			 y : 0,   // y offset (in pixels)
			 s : 0,   // spread
			 c : 0, // color
			 a : 0   // alpha // <- no comma on last item!
			 }
		     
		 },
		  
		   
		  labels  :  {
		  font  : "Roboto",
		  color : "#FFFFFF",
		  weight : "normal",
		  offset  : 5,  // Number of pixels to push the labels down away from numbers.
		  textScale : 1.0      // <- no comma on last item!
		 },
		 
		 
		  onComplete : countdownComplete,
		 
		  
			year	: rok,
			month	: miesiac, 
			day	: dzien,
			hour    :godzina,
			ampm : "pm",
			minute : "00",
			second : "00",
			width:300, 
		height:60,  
		rangeHi:"day",
			style   : "boring",
			 });

	   
				
			
	   

		   function countdownComplete(){
				 jQuery('#wizualizacja').show();
				 jQuery('#wizualizacja img').css('-webkit-filter','blur(4px) brightness(0.8)');
				 jQuery('#wizualizacja img').css('-moz-filter','blur(1px) brightness(0.8)');
				 jQuery('#wizualizacja img').css('-o-filter','blur(4px) brightness(0.8)');
				 jQuery('#wizualizacja img').css('-ms-filter','blur(4px) brightness(0.8)');
				 jQuery('#wizualizacja img').css('filter','blur(4px) brightness(0.8)');
				 jQuery('#wizualizacja img').css('filter','url(#svgBlur)');
				 jQuery('#wizualizacja img').addClass( "blur" );
				 jQuery('#div2').show();
				 
				 
				  }
    
    
    
</script>        
            </div>   
            
        </div>    
        </div>        
            </div>
          
        </section>    
       </div>
    </div>
    <div id="arrow-width">	
    <h3 class="hidden">Strzałka akcji up </h3>
	<div id="t-arrow">
		<a href="#lista1" onclick="pokazAlboUkryj('lista1'); return false;" id="lista2">
			<img class="strzalka" src="{{URL::asset('images/arrow2.png')}}">
		</a>
	</div>
</div> 
<a href="#"><div style="display: block;" id="toTop"></div></a>
   
<div id="misja-link"><a href="#misja">
    <div class="navbar-fixed-bottom" style="display: block;" id="toTop2"></div></a></div>
     <a href="#"><div  style="display: block;" id="toTop3"></div></a> 
     
    <div id="wizualizacja">
	 <div id="glass"></div> 
   <div id="container">
  
	<div style="position:relative;">
    <!-- bxSlider Javascript file -->
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.5/jquery.bxslider.js"></script>   
   <script type="text/javascript" src="{!! asset('js/zegar/js/jquery.sudoSlider.js') !!}"></script>           
    
    <ul class="bxslider" id="main-slider">
     <?php
        $i=0;
        while($i<=$iloscslajdow){?>
        <li><img id="photo_big1" src="<?=$liczbaPlikow[$i];?>" alt="image description" /></li>
              <?php  
            $i++;
        }
    ?>
    </ul>
       </div>
        </div>
    </div> 
     <!-- Stopka -->
<footer  id="wiz-demo"> 
		<section class="company container">
        <h3 class="hidden">Sekcja stopki</h3>    
       <div class="row">    
        <!--OFERS-->
     
        <div class="col-sm-12 col-md-6">
        <div class="company-ofers">
        <h2>Dane wizualizacji</h2>   
          <div class="akapit">  
      <p>Wizualizacja <strong>{{$stopka->nazwa}}</strong></p>
      <p>Rodzaj <span> {{$stopka->model}}</span></p>
      <p>Cena : <strong>{{$stopka->cena}}  zł</strong></p>
    @if($opis != '')          
      <p>Opis demo: {{str_limit($opis, $limit=80)}}</p> 
    @else
     <p>Brak opisu demo</p>
    @endif          
    <div class="bt-f btn-primary2"><p><a target="_blank" href="../../../filemanager/userfiles/demo/{{$dname}}/wycenastrony.pdf">Pobierz pełną wycenę</a></p>
    </div>  
    <p class="asked_q">Masz pytania lub uwagi? </p> 
     <p>Napisz na: <a href="mailto:BOK@GrupaAF.pl" target="_blank">BOK@GrupaAF.pl</a></p>        
    </div>     
   </div>  
         
         </div> 
    <div class="col-sm-12 col-md-6" >
     <div class="company-ofers2">   
    <h2>ZOBACZ, Z KIM WSPÓŁPRACUJESZ:</h2>
      <ul class="unstyled but-list">
         <li>
    <div class="bt-f btn-primary2"><p><a target="_blank" href="http://portfolio.grupaaf.pl/">Zobacz portfolio</a></p></div></li>
        <li>
    <div class="bt-f btn-primary2"><p><a target="_blank" href="http://grupaaf.pl/">Strona Agencji</a></p></div>       
    </li> <li>
    <div class="bt-f btn-primary2"><p><a target="_blank" href="https://www.facebook.com/Agencja-e-kreacji-Grupa-AF-315771558581298"><img src="{{URL::asset('images/fb.png')}}" alt="Agencja E-KREACJI - GRUPA AF" title="Agencja E-KREACJI - GRUPA AF"/>
    <span class="icon-fb">Agencja E-KREACJI - GRUPA AF </span></a></p></div>       
    </li> </ul>   
        </div>
         </div>
    </div>
   <!-- company-ofers-end -->
<div class="clearfix"></div>              
  <!--   Sekcja trust  -->

<!--  Sekcja logotypy  -->
<div class="ours-clients"> 
<h4 class="hidden"></h4>               
<div class="row">
<h3 class="text-center">Strategiczni Klienci</h3>
<ul>
<li class="col-xs-12 col-sm-6 col-md-3 graph"><a href="#"><img src="{{URL::asset('logotyp/logo01.png')}}" alt="logo01"></a></li>
<li class="col-xs-12 col-sm-6 col-md-3 graph"><a href="#"><img src="{{URL::asset('logotyp/logo06.png')}}" alt="logo06"></a></li>
<li class="col-xs-12 col-sm-6 col-md-3 graph"><a href="#"><img src="{{URL::asset('logotyp/logo03.png')}}" alt="logo03"></a></li>
<li class="col-xs-12 col-sm-6 col-md-3 graph"><a href="#"><img src="{{URL::asset('logotyp/logo02.png')}}" alt="logo02"></a></li>
<li class="col-xs-12 col-sm-6 col-md-3 graph"><a href="#"><img src="{{URL::asset('logotyp/logo04.png')}}" alt="logo04"></a></li>
</ul>   
    </div>
    </div> 
<div class="clearfix visible-xs-block"></div>
 <div class="row">
<!-- Sekcja nasze portfolio -->
<div class="portfolio">
<!-- tytul -->
<div class="section-title">
<div class="container" data-animate="bounceIn">
<h3>Topowe realizacje</h3>
</div>
</div>
<!-- slider -->
			<div class="portfolio-slider">
    <h4 class="hidden">Portfolio slider</h4>			
				<div class="container" data-animate="fadeIn">
					<div id="portfolio-carousel" class="owl-carousel">
					
						<div class="portfolio-item col-xs-12 col-sm-6 col-md-4">
				<img src="{{URL::asset('wizualizacje/wizualizacja14.jpg')}}" alt="HUNTI.EU" title="HUNTI.EU">
							<div class="portfolio-hover">
								<div class="portfolio-hover-inner">
									<h3>HUNTI.EU</h3>
									<p>Portal aukcyjny</p>
									<div class="btanchor"></div>
									
                                    
								</div>
							</div>
						</div>
						<div class="portfolio-item col-xs-12 col-sm-6 col-md-4">
				<img src="{{URL::asset('wizualizacje/wizualizacja2.jpg')}}" alt="KLIWO.PL" title="KLIWO.PL">
							<div class="portfolio-hover">
								<div class="portfolio-hover-inner">
									<h3>KLIWO.PL</h3>
									<p>Developer mieszkaniowy</p>
									<div class="btanchor"></div>
									
                                
								</div>
							</div>
						</div>
						<div class="portfolio-item col-xs-12 col-sm-6 col-md-4">
				<img src="{{URL::asset('wizualizacje/wizualizacja1.jpg')}}" alt="B-MOYA.PL" title="B-MOYA.PL">
							<div class="portfolio-hover">
								<div class="portfolio-hover-inner">
									<h3>B-MOYA.PL</h3>
									<p>Biuro projektów</p>
									<div class="btanchor"></div>
									
                                    
								</div>
							</div>
						</div>
						
							</div>
						</div>
            </div>                                                                                                      
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
     </div></div>
    </section>                                                                                                         </footer> 
    <!-- Sekcja wideo -->
          <div id="misja"  class="section-video">
        <h4 class="hidden">Nasza misja</h4>  
           <div class="container">
               <div class="row w2">
                   <div class="col-xs-12 col-md-1"></div> 
                    <div id="movie" class="col-xs-12 col-md-6">
        <iframe src="//www.youtube.com/embed/5ugTpy1pQ_E" frameborder="0" allowfullscreen></iframe>
                    </div>    
                    <div class="col-xs-12 col-md-4 r3">
                 <div id="manifest" class="manifest">
                     <h2>Manifest</h2>
                   <blockquote> <p class="box-misja1">
                       Wdrażamy najwyższy standard projektowania stron internetowych przy obsłudze <span class="txt-2">zrozumiałej dla Klienta. Jako agencja składająca się głównie z osób z niepełnosprawnością, włączając w to samego założyciela, chcemy promować wizerunek osób niepełnosprawnych jako ludzi zdolnych do konkurencji na otwartym rynku podejmując się najtrudniejszych zadań.</span></p>
                    </blockquote>
                     </div>
                   </div> 
                   <div class="col-xs-12 col-md-1"></div> 
               </div>
               </div>
            </div>
            
     <?php  header("Cache: clear");?>
   <div id="div2" class="col-xs-12">
        <span>
        Czas prezentacji wizualizacji upłynął.<br/>
Skontaktuj się ze swoim doradcą internetowym, aby przekazać mu swoją opinię.<br/>
Skontaktuj się również, jeżeli chcesz przedłużyć czas prezentacji. 
<br/>
<a href="mailto:BOK@GrupaAF.pl"> BOK@GrupaAF.pl   </a>     
        </span>
    </div>                 

<script type="text/javascript">
    jQuery(document).ready(function() {
        jQuery('.counter').counterUp({
            delay: 10,
            time: 1000
        });
        
    jQuery('#div2').hide();    
  
    });
</script>   
       </section>


<script type="text/javascript">
function pokazAlboUkryj(co) {
		 var obiekt = document.getElementById(co);
	   
			if (obiekt.style.display == 'block') {
			obiekt.style.display = 'none';
			 
			   $('#toolbarbg').show();
			   $('img.strzalka').css('margin-top','0px'); 
			   $("img.strzalka").fadeIn().animate({top:79}, 800);
			   $("#toolbarbg").fadeIn().animate({top:0}, 800);
			   } else {
			  obiekt.style.display = 'block';
			  $('#toolbarbg').show();
			  $("#toolbarbg").fadeIn().animate({top:-79}, 800);
			  $("img.strzalka").fadeIn().animate({top:0}, 800);
			  
			   }

			   }
    
    
    
    
  function Redirect (url) {
    var ua        = navigator.userAgent.toLowerCase(),
        isIE      = ua.indexOf('msie') !== -1,
        version   = parseInt(ua.substr(4, 2), 10);

    // Internet Explorer 8 and lower
    if (isIE && version < 9) {
        var link = document.createElement('a');
        link.href = url;
        document.body.appendChild(link);
        link.click();
    }

    // All other browsers
    else { window.location.href = url; }
}
    
 jQuery(document).ready(function($) {
     
      var sudoSlider = $("#slider").sudoSlider({
                effect: "fade",
				numeric:true,
				controlsShow:true,
				prevNext:true
                //pause: 3000,
				//auto:true,
				//prevNext:false
		   });
     
     setInterval(function() {
        var height = $('#wizualizacja').height();
  
       if(height==1028){
	        $('#toTop2').css('background-image','url(images/up.png)');
	   }
          
	   }, 5000); 
     
     
     
    var slider = $('#main-slider').bxSlider({
    pager: true,  
    pagerType: 'short',
    controls: true,
    mode: 'fade',
    responsive: true,
    adaptiveHeight: true,
    touchEnabled:true,
    });
     
//   var slideQty = slider.getSlideCount();    
        
        
     
     
 });
    
</script>





















<?php
function foldery($path){
$licznik=0;
$katalog = opendir($path); 
while($plik = readdir($katalog)){
    if ($plik<>'.' && $plik<>'..'){
    //zwiększenie licznika plików
       $licznik++;  
    }
    
}  
//zamknięcie katalogu
closedir($katalog);
return $licznik - 2;   
}

function foldery2($path){
 $i = 1;    
 $katalog = opendir($path);
 $plikiwynikowe=array();
while($plik = readdir($katalog))
{
 if ($plik<>'.' && $plik<>'..'){   
      //dodaje pliki do tablicy
       $plikiwynikowe[$i] = $path.$plik;
 $i++;
        }
    }
closedir($katalog);
  sort($plikiwynikowe);
  return $plikiwynikowe;     
}
?>








@stop