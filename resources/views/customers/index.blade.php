@extends('layouts.master')
@section('content')

<style type="text/css">
body {
	line-height: 1em;
}
body:before {
	content:"";
	height:100%;
	float:left;
	width:0;
	margin-top:-32767px;/
}    
    

</style>

<h3>Lista Klientów</h3>


   @if (Session::has('customer_add'))
    <div class="alert alert-success card">
        {{Session::get('customer_add')}}
    </div>
    @endif



    @if (Session::has('message_del'))
    <div class="alert alert-success card">
        {{Session::get('message_del')}}
    </div>
    @endif


 <div class="row clear-top">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
    
                <br/>
    <table class="table table-bordered table-striped">
    <thead>
        <tr>
        <th>Lp.</th>
        <th>Nazwa użytkownika</th>
        <th>E-mail</th>
        <th>Edytuj wskazane dane Klienta</th>
    @if(Auth::user()->canEdit())    
        <th>Usuń wskazanego Klienta</th> 
    @endif       
        </tr>
        </thead>
        <tbody>
    <tr>
        <div style="margin-bottom:20px">
        <a class="btn btn-success btn-sm" href="{{ action('DemosController@index')}}" >Wróć do wszytkich demo </a>
        <a class="btn btn-success btn-sm" href="customers/create" >Dodaj nowego Klient do listy </a> 
       </div>
    </tr>  
       <?php $i=1;?>
            
    @foreach($users as $user) 
      <tr>
      <th scope="row"><?php echo $i;?></th>    
        <td>
           {{$user->name}}    
        </td>    
          <td>
         {{$user->email}}
        </td>
         <td>
     <a href="{{ action('CustomersController@edit', $user->id) }}" class="btn btn-success btn-sm">
            Edytuj ustawienie Klienta
            </a>
          </td>
     @if(Auth::user()->canEdit())      
         <td>
     {{ Form::open(array('url' => 'customers/' . $user->id, 'class' => 'pull-right')) }}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Uwaga Usuwasz Klienta z listy', array('class' => 'btn btn-warning')) }}
        {{ Form::close() }}
          </td> 
    @endif      
       <?php $i++;?>
          </tr>
    @endforeach                 
        </tbody>
            </table>
            </div>
        </div>
     </div>
</div>





@stop