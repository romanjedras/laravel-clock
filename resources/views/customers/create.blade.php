@extends('layouts.master')
@section('content')

<style type="text/css">
body {
	line-height: 1em;
}
body:before {
	content:"";
	height:100%;
	float:left;
	width:0;
	margin-top:-32767px;/
}    
    
</style>

<h2> Dodaj nowego Klienta</h2>

  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">    
      <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>   
          </div>
        </div>
  </div>
</div>
 
 <div class="row content">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
           
              
               @if(count($errors) > 0)
           <div class="allert alert-danger">
               <ul>
               @foreach($errors->all() as $error) 
                  {{$error}}
                   <li>Pole jest wymagane</li>
                @endforeach   
               </ul>
                </div>
        @endif  
  
   {!! Form::open(['url'=>'store_customer','id'=>'create_clock','method'=>'post','classes'=>'form-horizontal']) !!}        
           
    {!! Form::hidden('user_role','customer') !!}       
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Nazwa klienta (Imię):'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Wstaw nazwę klienta')) !!}
    </div>
    </div>
  <div class="clearfix" style="padding-bottom:6px;"></div> 
 <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('email', 'Email Klienta: ')!!}
    </div>
    <div class="col-md-6">{!! Form::email('email',null,array('id'=>'email','required','class'=>'form-control','title'=>'Wstaw email klienta')) !!}
    </div>
    </div>
 <div class="clearfix" style="padding-bottom:6px;"></div> 
    <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('password', 'Podaj hasło dla Klienta: ')!!}
    </div>
    <div class="col-md-6">{!! Form::password('password', array('id'=>'password','required','class'=>'form-control','title'=>'Wstaw hasło klienta')) !!}
    </div>
    </div>
 <div class="clearfix" style="padding-bottom:25px;"></div>   
<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div>  
                                                             
           
    {!! Form::close() !!}       
           
           
            
        </div>
     </div>
</div>




















@stop