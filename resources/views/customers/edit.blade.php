@extends('layouts.master')
@section('content')

<style type="text/css">
body {
	line-height: 1em;
}
body:before {
	content:"";
	height:100%;
	float:left;
	width:0;
	margin-top:-32767px;/
}    
    
   </style>



<h2> Edytuj dane Klienta : {{$customer->name}} </h2>


<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body"> 

{!! Form::model($customer, ['method'=>'PATCH','class'=>'form-horizontal','action'=>['CustomersController@update', $customer->id]]) !!}  
{!! Form::hidden('user_role','customer') !!}

 <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Nazwa klienta (Imię):'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Zmień nazwę klienta')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div> 
  <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('email', 'Email Klienta: ')!!}
    </div>
    <div class="col-md-6">{!! Form::email('email',null,array('id'=>'email','required','class'=>'form-control','title'=>'Zmień email klienta')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div> 
   <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('password', 'Podaj hasło dla Klienta: ')!!}
    </div>
    <div class="col-md-6">{!! Form::password('password', array('id'=>'password','required','class'=>'form-control','title'=>'Zmień hasło klienta')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:25px;"></div>
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div> 



 {!! Form::close() !!}



            </div>
        </div>    
    </div>



@stop