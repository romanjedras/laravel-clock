<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Demo</title>
    </head>
    <body marginheight="0" topmargin="0" marginwidth="0" leftmargin="0" style="margin: 0; padding: 0; background-color: rgba(0,0,0,0.8);">

        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td colspan="3" style="height: 5px; background-color: #FFBE00"></td>
            </tr>
            <tr>
                <td colspan="3" style="height: 20px;"></td>
            </tr>
            <tr>
                <td style="width: 25px;"></td>
                <td style="font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 42px; color: #FFF;">
                     <a href="https://grupaaf.pl/" style="font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 18px; color: #FFF; text-decoration: none;"><img src="{{URL::asset('images/logo.png')}}" title="GrupaAF"></a>
                    <p>Witaj! </p> </td>
                <td style="width: 25px;"></td>
                </tr>
            <tr>
                <td colspan="3" style="height: 20px;"></td>
            </tr>
            <tr>
                <td colspan="3" style="height: 5px; background-color: #FFBE00;">
                    <p style="font-family: 'Trebuchet MS', Helvetica, sans-serif; color:#202020;">
                        {{$messages}} 
                    </p>    
                    <p>{{$messages2}} <a href="http://demo.grupaaf.pl/" style="font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 18px; color:#202020; text-decoration: none;">
                    www.demo.grupaaf.pl
                    </a> </p>    
                    <p style="font-family: 'Trebuchet MS', Helvetica, sans-serif; color:#202020;">   
                    Aby móc je zobaczyć, zaloguj się z następującymi danymi:
                    </p> 
                    <p style="font-family: 'Trebuchet MS', Helvetica, sans-serif; color:#202020;">Login / e-mail : {{$email}}, a Hasło to <span style="text-decoration: none;;color:#202020;">{{$haslo}}</span>  </p>
                    <p style="font-family: 'Trebuchet MS', Helvetica, sans-serif; color:#202020;">Z poważaniem,</p>
                    <p style="font-family: 'Trebuchet MS', Helvetica, sans-serif; color:#202020;">zespół agencji e-kreacji Grupa AF</p>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <tr>
                           
                            <td style="width: 25px;"></td>
                           
                            <td style="height: 90px; text-align: right;">
                                <a href="https://grupaaf.pl/" style="font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 18px; color: #FFF; text-decoration: none;">
                                    www.grupaaf.pl
                                </a>
                            </td>
                            <td style="width: 25px;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
