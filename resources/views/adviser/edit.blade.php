@extends('layouts.master')
@section('content')



<h2> Edytuj ustawienia {{$adviser->name}}</h2>





 <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
             
{!! Form::model($adviser, ['method'=>'PATCH','class'=>'form-horizontal','action'=>['AdviserController@update', $adviser->id]]) !!} 
{!! Form::hidden('user_id',$adviser->id) !!} 
<div class="clearfix" style="padding-bottom:6px;"></div>
    <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('Wybierz nazwę demo', 'Demo:'); !!}
    </div>
         <div class="col-md-6">
        {!! Form::select('demo',$demos, null ,['class'=>'form-control','title'=>'Wybierz demo']) !!}
    </div>
    </div>
<div class="form-group">
    <div class="col-md-4 control-label"> {!!  Form::label('name', 'Nazwa Doradcy:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Wybierz nazwę klienta')) !!}
    </div>
    </div> 
  <div class="clearfix" style="padding-bottom:25px;"></div>            
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div> 
   
{!! Form::close() !!}
            </div>
        </div>    
      </div>
</div>      



























@stop