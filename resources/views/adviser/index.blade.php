@extends('layouts.master')
@section('content')


 @if (Session::has('message_del'))
    <div class="alert alert-success card">
        {{Session::get('message_del')}}
    </div>
    @endif



<h2>Lista doradców zarejestrowanych w systemie</h2>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">   
    
                <br/>
    <table class="table table-bordered table-striped">
       <thead>
        <tr>
           <th>Lp.</th>
            <th>Nazwa Doradcy</th>
            <th>Email Doradcy</th>
            <th>Data utworzenia konta w systemie</th>
            <th>Akcja Edytuj dane </th>
            @if(Auth::user()->canEdit())
            <th>Akcja Usuwasz doradcę </th>
            @endif 
           </tr>    
        <tbody>
    <tr>
        <div style="margin-bottom:20px">
        <a class="btn btn-success btn-sm" href="{{ action('DemosController@index')}}" >Wróć do wszytkich demo </a> 
        <a class="btn btn-danger btn-sm" href="/register">Zarejestruj nowego doradcę </a> 
        </div>
    </tr>
        <?php $i=1;?>
            
    @foreach($advisers as $adviser) 
      <tr>
      <th scope="row"><?php echo $i;?></th>    
        <td>
           {{$adviser->name}}    
        </td>    
          <td>
        {{$adviser->email}}
        </td>
        <td>
        {{$adviser->created_at}} 
         </td>
          <td>
     <a href="{{ action('AdviserController@edit', $adviser->id) }}" class="btn btn-success btn-sm">
            Edytuj ustawienie Doradcy
            </a>
          </td>
    @if(Auth::user()->canEdit())      
         <td>
     {{ Form::open(array('url' => 'adviser/' . $adviser->id, 'class' => 'pull-right')) }}
        {{ Form::hidden('_method', 'DELETE') }}
        {{ Form::submit('Uwaga Usuwasz Doradcę z listy', array('class' => 'btn btn-warning')) }}
        {{ Form::close() }}
          </td> 
    @endif            
    <?php $i++;?>
          </tr>
    @endforeach            
        </tbody>
    </table>


            </div>
        </div>
    </div>
</div>
@stop