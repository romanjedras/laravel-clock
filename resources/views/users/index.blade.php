@extends('layouts.master')
@section('content')

<div class="box">
  
   <table class="table table-bordered table-striped">
      <thead>
      <tr>
       <th>Lp.</th>
       <th>Nazwa</th>
       <th>Email</th>
       <th>Rola</th>
       <th>Akcja: Edutuj ustawienia uzytkownika</th>    
      </tr>
      </thead> 
        <tbody>
    <?php $i=1;?>    
   @foreach($users as $user)
  <tr>
    <td><?php echo $i;?> </td>
  <td>
    {{$user->name}}
  </td>
  <td>
    {{$user->email}}
  </td>
  <td>
    {{$user->user_role}}
  </td>
  <td>
  <div class="links">
    @if(Auth::user()->canEdit())          
    <a class="btn btn-warning btn-xs red-tooltip" href="{{ url('users', $user->id) }}" rel="tooltip" title="edytuj ustawienia dla usera" data-toggle="tooltip" data-placement="right">Edytuj ustawienia usera</a>
 </div>
     @endif
      </td>
     <?php $i++;?>
    </tr>
  @endforeach 
    </tbody>
</table>


</div>


@stop