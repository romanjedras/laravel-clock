@extends('layouts.master')
@section('content')

<h2> Edytuj {{$user->name}} użytkownika</h2>


  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">    
     <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>    
          </div>
        </div>
  </div>
</div>


 <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
            
{!! Form::model($user, ['method'=>'PATCH','class'=>'form-horizontal','id'=>'edit_user','action'=>['UsersController@update', $user->id]]) !!}    
{!! Form::hidden('user_id',$user->id) !!} 
                   
  <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Nazwa uzytkownika (Imię):'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('name',null,array('id'=>'name','required','class'=>'form-control','title'=>'Zmień nazwę uzytkownika')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div> 
  <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('email', 'Email Użytkownika: ')!!}
    </div>
    <div class="col-md-6">{!! Form::email('email',null,array('id'=>'email','required','class'=>'form-control','title'=>'Zmień email klienta')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div> 
   <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('password', 'Podaj nowe hasło dla Użytkownika: ')!!}
    </div>
    <div class="col-md-6">{!! Form::password('password', array('id'=>'password','required','class'=>'form-control','title'=>'Zmień hasło użytkownika')) !!}
    </div>
    </div>
  <div class="clearfix" style="padding-bottom:25px;"></div>
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div> 
            </div>
        </div>    
      </div>
</div> 





@stop