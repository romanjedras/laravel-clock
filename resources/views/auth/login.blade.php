@extends('layouts.app')

@section('content')
<div class="container form-class">
    <div class="row text-center panel">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Logowanie</div>
                <div class="panel-body">
                    <form class="form-horizontal" id="user-login" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} e">
                            <label for="email" class="col-md-4 control-label pass-img">
                            <img src="{{URL::asset('images/user.png')}}" alt="GrupaAF" title="password">
                            </label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" placeholder="E-Mail Adres" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label pass-img">
                            <img src="{{URL::asset('images/password.png')}}" alt="GrupaAF" title="password">
                            </label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Hasło" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4 left">
                                <div class="checkbox">
                                    <label>
                            <input type="checkbox" id="remember" class="regular-checkbox big-checkbox" name="remember"><span>Pamiętaj mnie</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="przycisk">
                                   <span>Zaloguj się</span> 
                                </button>

<!--
                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    
                                </a>
-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
