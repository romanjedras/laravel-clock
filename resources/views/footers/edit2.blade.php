@extends('layouts.master')
@section('content')

<style type="text/css">
.form-group {
     margin-bottom: 2px; 
}

</style>



<h2> Edytuj {{$footer->demo}} stopkę</h2>
  
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">    
      <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>      
          </div>
        </div>
  </div>
</div>
  

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
            
{!! Form::model($footer, ['method'=>'PATCH','class'=>'form-horizontal','action'=>['FootersController@update', $footer->id]]) !!}             
    {!! Form::hidden('user_id',$user_id) !!}
      
 <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('date', 'Dzien dodania:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('date',null,array('id'=>'date','required','class'=>'form-control','title'=>'Wybierz dzień')) !!}
    </div>
    </div>
    
 <div class="form-group">
        <div class="col-md-4 control-label"> {!! Form::label('nazwa', 'Nazwa projektu:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('nazwa',null,array('id'=>'nazwa','required','class'=>'form-control','title'=>'Wybierz nazwę')) !!}
    </div>
    </div>
  
 <div class="clearfix" style="padding-bottom:6px;"></div> 
 <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('model', 'Model projektu:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::select('model', $categories, null,['class'=>'form-control','title'=>'Wybierz model strony']) !!}  
    </div>
    </div>
    
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('cena', 'Cena projektu:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('cena',null,array('id'=>'cena','required','class'=>'form-control','title'=>'Wybierz cena')) !!}
    </div>
    </div>
           
     <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('Wybierz nazwę demo', 'Demo:'); !!}
    </div>
         <div class="col-md-6">
        {!! Form::text('demo', null,array('id'=>'demo','required','class'=>'form-control','title'=>'Wybierz demo')) !!}
    </div>
    </div>            
                
    <br/>
     <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div>           
   
    
{!! Form::close() !!}
            </div>
        </div>    
      </div>
</div> 
@stop