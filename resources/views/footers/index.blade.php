@extends('layouts.master')
@section('content')


<style type="text/css">

    .table tbody > tr{
     font-size:12px;
    white-space: nowrap;
    }
    
    
</style>


<h2>Lista wszystkich footer wraz z przypisanym demo 2</h2>

  @if (Session::has('footers_created'))
    <div class="alert alert-success card">
        {{Session::get('footers_created')}}
    </div>
    @endif


<div class="row">
    <div class="col-md-10 text-center">
        <div class="card">
            <div class="panel-body">   
    <div style="margin-bottom:20px; float: left;">
         <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>  
        <a class="btn btn-danger btn-sm" href="footers/create">Dodaj nowy wpis do stopki </a> 
        </div>
                <br/>
    <table class="table table-bordered table-striped">
       <thead>
        <tr>
           <th>Lp.</th>
            <th>Nazwa demo</th>
            <th>Data dodania</th>
            <th>Nazwa</th>
            <th>Model projektu</th>
            <th>Cena projektu</th>
            <th>Użytkownik</th>
        <th>Edytuj zapis projektu</th>
        </tr>
        </thead>
        <tbody>
    <?php $i=1;?>
            
    @foreach($footers as $footer) 
      <tr>
      <th style="margin:0 auto;" scope="row"><?php echo $i;?></th>    
        <td>
            <h4>{{$footer->demo}}</h4>    
          
        </td>    
        <td>
        <p class="col-md-3"> {{$footer->date}}</p>
        </td>        
         <td>
        <p class="col-md-3"> {{$footer->nazwa}}</p>
        </td>     
         <td>
        <p class="col-md-3"> {{$footer->model}}</p>
        </td>     
         <td>
        <p class="col-md-3"> {{$footer->cena}}</p>
        </td>
          @foreach($users as $user)
         @if($footer->user_id == $user->id)
        <td>
        <p class="col-md-3"> {{$user->name}}</p>
        </td>  
        @endif                
           @endforeach  
          <td>
     <a href="{{ action('FootersController@edit', $footer->id) }}" class="btn btn-success btn-sm">
            Edytuj stopkę
                    </a>
          </td>
    <?php $i++;?>
          </tr>
    @endforeach               
            
            
        
       
        </tbody>
    </table>        
                    
                                    
    
            </div>
        </div>    
      </div>
</div> 












@stop