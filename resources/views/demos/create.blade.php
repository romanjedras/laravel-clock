@extends('layouts.master')
@section('content')

<style type="text/css">
body { padding-bottom: 70px; }
</style>

<h2> Dodaj nowe demo </h2>


<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">
              
       

                
{!! Form::open(['url'=>'store2','id'=>'create_demo','method'=>'post','classes'=>'form-horizontal']) !!}
@include('demos.form_errors')
{!! Form::hidden('user_id',$user_id) !!}
           <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('name', 'Podaj nazwę demo:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('name',null,array('id'=>'demo','class'=>'form-control','title'=>'Wstaw nazwę demo')) !!}
    </div>
    </div>                
  <div class="clearfix" style="padding-bottom:6px;"></div>
           <div class="form-group">
        <div class="col-md-12 control-label"> {!!  Form::label('description', 'Podaj opis demo:'); !!}
    </div>
    <div class="col-md-12">
    
     <script type="text/javascript" src="{!! asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') !!}"></script>
    <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
    {!! Form::textarea('description',null,array('id'=>'description','name'=>'description')) !!}
     <script>
//        CKEDITOR.replace( 'demo_description' );
       CKEDITOR.replace('description', {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
  });
    </script>            
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div>
<!--
   <div class="form-group">
    <div class="col-md-4 control-label"> {!!  Form::label('CategoryList', 'Wybierz kategorię demo:'); !!}
    </div>

         <div class="col-md-6">
    {!! Form::select('CategoryList', $categories, null,['class'=>'form-control','title'=>'Wybierz kategorie demo']) !!}
    </div>
-->
<!--    </div>             -->
    
     <div class="clearfix" style="padding-bottom:25px;"></div>                           
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Dodaj demo',['class'=>'btn btn-success']);!!}
        
    </div>
    </div>
                
{!! Form::close() !!}
            </div>
        </div>
        </div>
    </div>

<div class="clearfix" style="padding-bottom:50px;"></div>
@stop