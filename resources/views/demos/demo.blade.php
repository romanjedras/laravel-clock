@extends('layouts.master')
@section('content')
 
   
   
  
<style type="text/css">
  
    .card-content a, .card-content a:hover {
    color: #FFF;
    text-decoration: none;
}

    table{
        padding:0 20px; 
        margin: 0 auto;
    }
    
    table tr{
       margin: 0 auto; 
    }
    
     a{
     margin-right:6px;
    float:left;
    }
    
    
a.active-link {
    text-decoration: none;
    color: #333;
    display: block;
    margin-bottom: 20px;
    float: none;
}  
    
section.header {
  left: 5%;
    position: relative;
    margin: 0 auto;
}    
  
a.position_tooltip.asTooltip_active {
    display: block;
    }
    
.col-md-6 {
    width: 49%;
    margin-right: 11px;
    height: 310px;
} 
    
#clock3{
 text-align: right;
    }    
    
</style>






<div class="videos-header card">
    
   <a href="demos/create" class="btn btn-primary btn-sm" rel="tooltip"  title="Dodaj nowe demo">Dodaj nowe demo</a>
    <a href="clocks/create" class="btn btn-primary btn-sm" rel="tooltip" title="Dodaj nowy czas">Dodaj nowy czas</a>
    <a href="footers/create" class="btn btn-primary btn-sm" title="Dodaj nowa stopkę">Dodaj nowa stopkę</a>
   
     <section class="header">
       
       <h2> Lista wszystkich utworzonych demo </h2> 
       
<!--         <h4>Zalogowałeś się jako : </h4>-->
    </section>
</div>
<p id="clock3"></p> 
 
  <div class="box">
   
 <?php $i=0;?>
    
    @foreach($demos as $demo)
      
    <?php if($i % 3 == 0) : ?>
    <div class="row">
    <?php endif; ?>
    <div class="col-md-6 card">
       <div class="post-news">
<a href="{{ action('DemosController@edit', $demo->id)}}" class="position_tooltip active-link" data-position_value="top left" rel="tooltip" title="Kliknij tu aby edytować demo" data-toggle="tooltip" data-placement="right">
         <h4>{{$demo->name}}</h4> 
           </a>   
        </div>
    @foreach($cloks as $clok)
        @if ($clok->demo === $demo->name)
        <span>
        <?php echo 'Ustawiona godzina : ' ?> {{$clok->godzina}} <?php echo 'Dzień : ' ?>{{$clok->dzien}}
        <?php echo 'Miesiac : ' ?>{{$clok->miesiac}}<?php echo 'Rok :' ?>{{$clok->rok}}</span>
       
          
           @endif
        @endforeach  
        @foreach($footers as $footer)
        @if ($footer->demo === $demo->name)
        <p><?php echo 'Ustawiona Nazwa : ' ?> {{$footer->nazwa}} </p>
        <p><?php echo 'Ustawiony Model : ' ?> {{$footer->model}}</p>
        <p><?php echo 'Ustawiona Cena : ' ?> {{$footer->cena}}</p>
       @endif
        @endforeach        
            
                
        <div class="post-content">
        <p>{{str_limit($demo->description,$limit=80)}}</p>  
        </div> 
        <div class="post-content">
        <h4>Demo wystawił : {{$demo->user->name}}</h4>
        </div>
        <div class="links">
    <a class="btn btn-warning btn-xs red-tooltip" href="{{ url('clocks', $demo->name) }}" rel="tooltip" title="dodaj czas do demo" data-toggle="tooltip" data-placement="right">Dodaj czas do demo</a>
       
       <a class="btn btn-danger .danger btn-xs" href="{{ url('footers', $demo->name) }}" rel="tooltip" title="dodaj stopkę do demo" data-toggle="tooltip" data-placement="right">Dodaj stopke do demo</a>
       
        <a class="btn btn-success btn-xs" href="http://localhost:81/filemanager/" rel="tooltip" title="media" data-toggle="tooltip" data-placement="right">Dodaj źródła do mediów</a>
        
        <a class="btn btn-info btn-xs" href="{{ url('demos2', $demo->name) }}">Zobacz wizualizacje</a>
        
        
        <br/>
        </div>
        </div>    

 <?php if($i != 0 && $i % 3 == 0) : ?>
                    </div><!--/.row-->
                <?php endif; ?>

        <?php $i++; ?>
@endforeach

    </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>                 
                       
       <script type="text/javascript">
       $(document).ready(function() {
                            $("body").asTooltip({
                                show: { target: '.position_tooltip' },
                                hide: { target: '.position_tooltip' }
                            });
            $.clock.locale = {"pl":{"weekdays":["Niedziela","Poniedziałek","Wtorek", "Środa","Czwartek","Piatek","Sobota", ],"months":["Styczeń","Luty","Marzec","Kwiecień", "Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad", "Grudzień"] } };
           
           $("#clock3").clock({"langSet":"pl"});
           
        customtimestamp = new Date();
      customtimestamp = customtimestamp.getTime();
      customtimestamp = customtimestamp+1123200000+10800000+14000;
           
                        });
      

                    </script>
@stop