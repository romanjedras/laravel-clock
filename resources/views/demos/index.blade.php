@extends('layouts.master')
@section('content')
<style type="text/css">
body {
	line-height: 1em;
}
body:before {
	content:"";
	height:100%;
	float:left;
	width:0;
	margin-top:-32767px;/
}    
    
a{
    text-transform: none;
    color: #000;
    font-weight: bold;
    }    
 a:active,a:hover {
    color: #525252;
    text-decoration: none;
}
    
.row.videos-header.card ul {
    list-style: none;
}    
</style>
<?php 
 $value = Auth::user()->id;
setcookie ("Use", $value,time()+3600);  ?>

<h2> Lista wszystkich utworzonych demo w formie jednej listy</h2>

    @if (Session::has('demo_created'))
    <div class="alert alert-success card">
        {{Session::get('demo_created')}}
    </div>
    @endif
    
     @if (Session::has('demo_request'))
    <div class="alert alert-warning card">
        {{Session::get('demo_request')}}
    </div>
    @endif
    

<div class="row videos-header card">
   <ul>
       <li class="col-md-4">
    <a href="demos/create" class="btn btn-primary btn-sm" rel="tooltip"  title="Dodaj nowe demo">Dodaj nowe demo</a>   
       </li>
       <li class="col-md-4">
    <a href="clocks/create" class="btn btn-primary btn-sm" rel="tooltip" title="Dodaj nowy czas">Dodaj nowy czas</a>   
       </li>
       <li class="col-md-4">
    <a href="footers/create" class="btn btn-primary btn-sm" title="Dodaj nowa stopkę">Dodaj nowa stopkę</a>   
       </li>
   </ul>
    </div>    

<div class="row">
    <div class="col-md-6">
    <p id="clock3"></p>     
    </div>
    <div class="col-md-6">
    <p>Witam : {{$user}}</p>
    </div>
</div>






<div class="row">
<div class="col-md-10">
  <table class="table table-bordered table-striped">
  <thead>
        <tr>
           <th>Lp.</th>
            <th>Nazwa demo</th>
            <th>Opis</th>
       <th>Ustawienie i Podglad Wizualizacji</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=1;?>
        @foreach($demos as $demo)    
      <tr>
        <td><?php echo $i;?> </td>
          <td>
          <div class="post-news">
    <a class="position_tooltip active-link" data-position_value="top left" rel="tooltip" href="{{ url('demos', $demo->id) }}" title="Kliknij tu aby zobaczyć szczegóły demo" data-toggle="tooltip" data-placement="right">{{$demo->name}}</a>
              </div>
          </td> 
          <td>{{str_limit($demo->description,$limit=80)}}</td>
          <td>
              <div class="links">
        
    <a class="btn btn-warning btn-xs red-tooltip" href="{{ url('clocks', $demo->name) }}" rel="tooltip" title="dodaj czas do demo" data-toggle="tooltip" data-placement="right">Edytuj czas do demo</a>
       
    <a class="btn btn-danger .danger btn-xs" href="{{ url('footers', $demo->name) }}" rel="tooltip" title="dodaj stopkę do demo" data-toggle="tooltip" data-placement="right">Edytuj stopke do demo</a>
   
    
    <a class="btn btn-danger .danger btn-xs" href="{{ action('DemosController@show3',$demo->id) }}" rel="tooltip" title="dodaj stopkę do demo" data-toggle="tooltip" data-placement="right">Kategoria</a>      
       
    <a class="btn btn-success btn-xs" href="/filemanager/" rel="tooltip" title="media" data-toggle="tooltip" data-placement="right">Dodaj źródła do mediów</a>
        
    <a class="btn btn-info btn-xs" href="{{ url('demos2', $demo->name) }}">Zobacz wizualizacje</a>
    </div>
          </td> 
      <?php $i++;?>
          </tr>
    @endforeach
                      
      </tbody>   
    </table>
   </div>
    </div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>                 
                       
       <script type="text/javascript">
       $(document).ready(function() {
                            $("body").asTooltip({
                                show: { target: '.position_tooltip' },
                                hide: { target: '.position_tooltip' }
                            });
            $.clock.locale = {"pl":{"weekdays":["Niedziela","Poniedziałek","Wtorek", "Środa","Czwartek","Piatek","Sobota", ],"months":["Styczeń","Luty","Marzec","Kwiecień", "Maj","Czerwiec","Lipiec","Sierpień","Wrzesień","Październik","Listopad", "Grudzień"] } };
           
           $("#clock3").clock({"langSet":"pl"});
           
        customtimestamp = new Date();
      customtimestamp = customtimestamp.getTime();
      customtimestamp = customtimestamp+1123200000+10800000+14000;
           
                        });
      

                    </script>



@stop