@extends('layouts.master')
@section('content')

<h2> Edytuj {{$demo->name}} stopkę</h2>
  
  
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">    
     <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>    
          </div>
        </div>
  </div>
</div>

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
            
{!! Form::model($demo, ['method'=>'PATCH','class'=>'form-horizontal','id'=>'edit_demo','action'=>['DemosController@update', $demo->id]]) !!}             
    {!! Form::hidden('user_id',$user_id) !!}
      
   <div class="form-group">
        <div class="col-md-4 control-label2a"> {!!  Form::label('name', 'Podaj nazwę demo:'); !!}
    </div>
         <div class="col-md-6">
{!! Form::text('name',null,array('id'=>'demo')) !!}
    </div>
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div>                 
  <div class="form-group">
        <div class="col-md-12 control-label2"> {!!  Form::label('description', 'Podaj opis demo:'); !!}
    </div>
    <div class="col-md-12">
     <script src="../../vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
    {!! Form::textarea('description',null,array('id'=>'description','name'=>'description')) !!}
     <script>
//        CKEDITOR.replace( 'demo_description' );
       CKEDITOR.replace('description', {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
  });
    </script>           
      </div>            
    </div>
    <div class="clearfix" style="padding-bottom:6px;"></div>  
     <div class="form-group">
    <div class="col-md-4 control-label"> {!!Form::label('CategoryList', 'Wybierz kategorię demo:'); !!}
    </div>
         <div class="col-md-6">
{!!Form::select('CategoryList', $categories, null,['class'=>'form-control','title'=>'Wybierz kategorie demo']) !!}
    </div>
    </div> 
    <div class="clearfix" style="padding-bottom:25px;"></div> 
     <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div>           
   
    
{!! Form::close() !!}
            </div>
        </div>    
      </div>
</div> 
@stop