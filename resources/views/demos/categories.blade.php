@extends('layouts.master')
@section('content')

 <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">   
      <a class="btn btn-danger btn-sm" href="{{ action('CategoriesController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do kategorii </a>               
     <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>  
      
          </div>
        </div>
  </div>
</div>


<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">

      
        <p>Demo należy do kategorii : {{$category}}</p>
        

            </div>
        </div>
    </div>
</div>    

@stop