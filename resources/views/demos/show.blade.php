@extends('layouts.master')
@section('content')

<div style="margin-bottom:20px">
        <a class="btn btn-success btn-sm" href="{{ action('DemosController@index')}}" >Wróć do wszytkich demo </a> 
</div>

 <div class="row">
<div class="col-sm-12 card">

<div class="post-news">
<a href="{{ action('DemosController@edit', $demo->id)}}" class="position_tooltip active-link" data-position_value="top left" rel="tooltip" title="Kliknij tu aby edytować demo" data-toggle="tooltip" data-placement="right">
  <h4>Nazwa demo: {{$demo->name}}</h4> 
</a>
</div>
   
@if ($times->demo === $demo->name)
 <span>
    <?php echo 'Ustawiona godzina : ' ?> {{$times->godzina}} <?php echo 'Dzień : ' ?>{{$times->dzien}}
    <?php echo 'Miesiac : ' ?>{{$times->miesiac}}<?php echo 'Rok :' ?>{{$times->rok}}</span>

@endif


@if($footer->demo === $demo->name)
        <p><?php echo 'Ustawiona Nazwa : ' ?> {{$footer->nazwa}} </p>
        <p><?php echo 'Ustawiony Model : ' ?> {{$footer->model}}</p>
        <p><?php echo 'Ustawiona Cena : ' ?> {{$footer->cena}}</p>
       @endif

 <div class="post-content">
        <p>{{$demo->description}}</p> 
        <p>Demo dodał :{{$auth->name}}</p> 
        <p>Email użytkownika : {{$auth->email}}</p>
        </div>
</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>                 
                       
       <script type="text/javascript">
       $(document).ready(function() {
                    $("body").asTooltip({
                         show: { target: '.position_tooltip' },
                         hide: { target: '.position_tooltip' }
                            });

       });
    </script>       

@stop