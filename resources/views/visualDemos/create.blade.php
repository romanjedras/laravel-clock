@extends('layouts.master')
@section('content')



<h2> Dodaj wizualizacje </h2>
  
  
  
  
  
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">    
     <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>   
          </div>
        </div>
  </div>
</div>

  <div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
            {!! Form::open(['url'=>'store_visual','id'=>'create_visual','method'=>'post','classes'=>'form-horizontal']) !!}
            @include('visualDemos.form_errors')
            <div class="clearfix" style="padding-bottom:6px;"></div>
             <div class="form-group">
            <div class="col-md-4 control-label">{!! Form::label('title', 'Nazwa wizualizacji:');!!}
            </div>
            <div class="col-md-6">
        {!! Form::text('title',null,array('id'=>'title','class'=>'form-control','title'=>'Wstaw nazwę wizualizacji')) !!}
            </div>
            </div> 
             <div class="clearfix" style="padding-bottom:6px;"></div>
             <div class="form-group">
            <div class="col-md-4 control-label">{!! Form::label('description', 'Dodaj opis:');!!}
            </div>
            <div class="col-md-6">
        {!! Form::text('description',null,array('id'=>'description','class'=>'form-control','title'=>'Wstaw opis wizualizacji')) !!}
            </div>
            </div> 
             <div class="clearfix" style="padding-bottom:6px;"></div>
             <div class="form-group">
            <div class="col-md-4 control-label">{!! Form::label('url', 'Adres wizualizacji:');!!}
            </div>
            <div class="col-md-6">
            {!! Form::text('url',null,array('id'=>'url','class'=>'form-control','title'=>'Wstaw adres wizualizacji')) !!}
                 </div>
            </div> 
            <div class="clearfix" style="padding-bottom:6px;"></div> 
             <div class="form-group">
            <div class="col-md-4 control-label"> {!!  Form::label('customer_id', 'Wybierz nazwę Klienta:'); !!}
            </div>
            <div class="col-md-6">
           <select id="customer_id" name="customer_id" class="form-control" title="Wybierz nazwę Klienta">
            @foreach($customers as $customer)
            <option value="{{$customer->name}}">{{$customer->name}}</option>
            @endforeach
            </select>
            </div>
            </div>
        <div class="clearfix" style="padding-bottom:6px;"></div>          
             <div class="form-group">
            <div class="col-md-4 control-label"> {!!  Form::label('demo', 'Demo:'); !!}
            </div>
            <div class="col-md-6">
            {!! Form::select('demo', $demos, 'demo1',['class'=>'form-control','title'=>'Wybierz demo']) !!}
            </div>
            </div>            
             <div class="clearfix" style="padding-bottom:25px;"></div>
             <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
            </div>
            </div>           
           {!! Form::close() !!}
            
            </div>
        </div>
      </div>
</div>


@stop