@extends('layouts.master')
@section('content')



<div style="margin-bottom:20px">
<a class="btn btn-success btn-sm" href="{{ action('VisualDemosController@index')}}" >
Wróć do wszytkich wizualizacji </a> </div>

<h2> Edytuj {{$visual_demos->title}} wizualizacji</h2>

<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">
            
    {!! Form::model($visual_demos, ['method'=>'PATCH','class'=>'form-horizontal','action'=>['VisualDemosController@update', $visual_demos->id]]) !!}
    @include('visualDemos.form_errors')             
    {!! Form::hidden('user_id',$user_id) !!} 
    <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('title', 'Wstaw tytuł:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('title',null,array('id'=>'title','required','class'=>'form-control','title'=>'Wstaw tytuł')) !!}
    </div>
    </div>             
    <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('customer_id', 'Wstaw nazwę Klienta:'); !!}
    </div>
    <div class="col-md-6">
   <select id="customer_id" name="customer_id" class="form-control" title="Wybierz nazwę Klienta">
    @foreach($users as $user)
    <option value="{{$user->name}}">{{$user->name}}</option>
    @endforeach
    </select>
  </div>
    </div>
   <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('url', 'Wstaw adres:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('url',null,array('id'=>'url','required','class'=>'form-control','title'=>'Wstaw adres')) !!}
    </div>
    </div>
       <div class="form-group">
        <div class="col-md-4 control-label"> {!!  Form::label('description', 'Wstaw opis:'); !!}
    </div>
         <div class="col-md-6">
    {!! Form::text('description',null,array('id'=>'description','required','class'=>'form-control','title'=>'Wstaw opis')) !!}
    </div>
    </div>
     <div class="clearfix" style="padding-bottom:6px;"></div>          
    <div class="form-group">
    <div class="col-md-4 control-label"> {!!  Form::label('demo', 'Wybierz demo:'); !!}
    </div>
    <div class="col-md-6">
    {!! Form::select('demo', $demos, $visual_demos->demo,['class'=>'form-control','title'=>'Wybierz demo']) !!}
    </div>
    </div>            
    <div class="clearfix" style="padding-bottom:25px;"></div>            
    <div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit('Zapisz',['class'=>'btn btn-success']);!!}
    </div>
    </div>           
   {!! Form::close() !!}      
                   
            </div>
        </div>
    </div>
</div>






















@stop