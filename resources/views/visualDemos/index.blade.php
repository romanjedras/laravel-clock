@extends('layouts.master')
@section('content')

  @if (Session::has('visualdemos_created'))
    <div class="alert alert-success card">
        {{Session::get('visualdemos_created')}}
    </div>
    @endif
    
    @if (Session::has('visualdemos_update'))
    <div class="alert alert-success card">
        {{Session::get('visualdemos_update')}}
    </div>
    @endif
    
    
    
    

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="panel-body">    
     <a class="btn btn-warning btn-sm" href="{{ action('DemosController@index')}}" rel="tooltip" title="wróć do demo" data-toggle="tooltip" data-placement="right">Wróć do wszytkich demo </a>           
    <a class="btn btn-success btn-sm" href="{{ action('ClocksController@index')}}" rel="tooltip" title="wszystkie zegary" data-toggle="tooltip" data-placement="right">Zobacz wszystkie zegary </a>
     <a class="btn btn-danger btn-sm" href="{{ action('FootersController@index')}}" rel="tooltip" title="przejdź do stopek" data-toggle="tooltip" data-placement="right">Przejdź do ustawień stopek </a>   
          </div>
        </div>
  </div>
</div>



<h2>Lista wszytkich ustawionych wizualizacji</h2>


<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <div class="panel-body">   
    
                <br/>
<table class="table table-bordered table-striped">
       <thead>
        <tr>
           <th>Lp.</th>
            <th>Nazwa wizual</th>
            <th>Nazwa Klienta</th>
            <th>Adres</th>
            <th>Opis</th>
            <th>Demo</th>
           @if(Auth::user()->canEdit())
            <th>Edytuj wizualizacje</th>
            @endif
        </tr>
        </thead>
        <tbody>
    <tr>
        <div style="margin-bottom:20px">
       <a class="btn btn-danger btn-sm" href="{{ action('VisualDemosController@create')}}">Dodaj nowa wizualizacje </a> 
        </div>
    </tr>
  <?php $i=1;?>
    @foreach($visual_demos as $visual_demo)    
        <tr>
      <th scope="row"><?php echo $i;?></th>
       <td>{{$visual_demo->title}}</td>
       <td>{{$visual_demo->customer_id}}</td> 
       <td>{{$visual_demo->url}}</td>
       <td>{{str_limit($visual_demo->description,$limit=80)}}</td>
       <td>{{$visual_demo->demo}}</td>
       @if(Auth::user()->canEdit())
         <td>
        <a href="{{ action('VisualDemosController@edit',$visual_demo->id)}}" class="btn btn-success btn-sm">
            Edytuj wizualizacje
            </a></td>
        @endif     
        </tr>
    <?php $i++;?>    
    @endforeach    
   
    </tbody>
        
    </table>





@stop