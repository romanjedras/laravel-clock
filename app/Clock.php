<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clock extends Model
{
    protected $fillable = ['user_id','demo_id','godzina','dzien','miesiac','rok','demo'];
    
    
  public function user(){
        
     return $this->belongsTo('App\User');   
    }
    
    
    public function demo(){
        return $this->belongsTo('App\Demos');   
    }
    
    
     /** Każde demo może należeć do wybranej kategorii**/
    public function categories(){
        return $this->belongsToMany('App\Category')->withTimestamps();
    }
    
    /** Lista Id kategorii dla jednego demo**/
    public function getCategoryListAttribute(){
        return $this->categories->pluck('id')->all();
    }
        
    
    
}
