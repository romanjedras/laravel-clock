<?php

namespace App;
use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function demos(){
        return $this->hasMany('App\Demo');
    }
    
    public function clocks(){
        return $this->hasMany('App\Clock');
    }
    
    public function footers(){
        return $this->hasMany('App\Footer');
    }
    
    public function visualdemos(){
        return $this->hasMany('App\VisualDemo');
    }
    
    public function categories(){
        return $this->hasMany('App\Category');
    }
    
    public function canEdit(){
        if(Auth::user()->user_role == 'super_admin'){
        return 1;    
        }else{
        return 0;    
        }
       
    }
    
}
