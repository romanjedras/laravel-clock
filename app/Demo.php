<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demo extends Model
{
    //
    protected $fillable = [
        'user_id', 'name', 'description'
    ];
    
    
    /**Użytkownik zalogowany może dodać nowe demo do systemu**/
    
    public function user(){
        
     return $this->belongsTo('App\User');   
    }
    
    
    /** Każde demo może należeć do wybranej kategorii**/
    public function categories(){
        return $this->belongsToMany('App\Category')->withTimestamps();
    }
    
    /** Lista Id kategorii dla jednego demo**/
    public function getCategoryListAttribute(){
        return $this->categories->pluck('id')->all();
    }
}
