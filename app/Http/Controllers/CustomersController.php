<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CrtCustomerRequest;
use Auth;
use App\User;
use Session;
use Mail;


class CustomersController extends Controller
{
   
    /** Konstruktor włacza środowisko ochronne (blokade dostepu dla osob nie zalogowanych) **/ 
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    //**Index method to display collection Customer**/
    public function index(){
        
        $users = User::where('user_role','=','customer')->get();
        
   return view('customers/index')->with('users',$users);
       
   }
    
/**Metoda prezentuje formularz dodania nowego Customer do kolekcji**/  
    
    public function create(){ 
        
    return view('customers/create');   
    
    }
    
     /**
     * [[Method save new Customer to Collection]]
     * @param  [[Type]] Request $request [[Description]]
     * @return [[Type]] [[Description]]
     */
    public function store(CrtCustomerRequest $request){
        
        $user_email = $request->get('name');
         $email = $request->get('email'); 
         $haslo = $request->get('password');
         $messages = 'Drogi Kliencie,';
$messages2 ='agencja e-kreacji Grupa AF przygotowała dla Ciebie wizualizacje projektu. Są one dostępne pod adresem:';
        
         
       if(!empty($user_email) && !empty($email)){
Mail::send('emails.reminder', ['user' => $user_email,'messages'=>$messages,'email'=>$email,'haslo'=>$haslo,'messages2'=>$messages2], function ($m) use ($user_email,$email) {
            $m->from('romanjedras@prokonto.pl', 'CMS Demo');

            $m->to($email, $user_email)->subject('Twoja wiadomość');
        });  
        }
        
        $user = new User;
           
        $data['name'] = $user->name=$request->name; 
        $data['email'] = $user->email=$request->email;
        $data['user_role'] = $user->user_role=$request->user_role;
        $user->password =bcrypt($request->password);
        $user->remember_token = str_random(100);
        
        
        if($user->save()){
        return redirect('customers');
        Session::flash('customer_add','Klient zostal dodany do systemu');    
         }
        
    }
    
     /**
     * [[Method display form edit]]
     * @param  [[Type]] $id [[Description]]
     * @return [[View form]] [[Description]]
     */
    public function edit($id){
    $customer = User::findOrFail($id);    
     if(!empty($customer)){
       return view('customers/edit')->with('customer',$customer); 
     }else{
    return redirect('customers'); 
     }
        return 1;
    }
    
    /**Metoda aktualizuje dane wybranego Customer w kolekci**/      
                          
     public function update($id, Request $request){
         
         $this->validate($request, [
        'name' => 'required|max:255',
       'password' => 'required|min:6|max:12'     
       ]); 
         
         $user = User::findOrFail($id);
        $data['name'] = $user->name=$request->name; 
        $data['email'] = $user->email=$request->email;
        $data['user_role'] = $user->user_role=$request->user_role;
        $user->password =bcrypt($request->password);
        $user->remember_token = str_random(100);
         
        if($user->update()){
        return redirect('customers');  
        }
    }
    
  /**Metoda display dependences ones Customers**/    
    public function show($id){
        $user = User::findOrFail($id);
        return view('customers/show')->with('user',$user); 
    } 
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    $user = User::find($id);
        
    if(!empty($user)){
        $user->delete();
    }    
     Session::flash('message_del', 'Suksesywnie usunełeś Klienta!');   
     return redirect('customers');    
    }
    
    
}
