<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateLoginRequest;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
   
    
        
        
    switch(Auth::user()->user_role){
        case 'customer': 
        return view('customer')->with('id',Auth::user());
        break;    
        default:
        return view('home');  
        }   
        
    }
    
    public function logout(){
        return view('auth.login');
    }
}
