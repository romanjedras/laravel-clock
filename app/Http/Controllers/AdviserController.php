<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Demo;
use App\User;
use Session;
use App\Clock;
use App\Footer;

class AdviserController extends Controller
{
    /** Konstruktor włacza środowisko ochronne (blokade dostepu dla osob nie zalogowanych) **/ 
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
  /** Metoda przekazuje dane do widoku -> index **/

    public function index(){
       
    if(Auth::user()->user_role === 'super_admin' || Auth::user()->user_role === 'doradca'){  
    
        if(Auth::user()->user_role === 'super_admin'){
            $demos = Demo::latest()->get();
            $adviser = User::where('user_role','=','doradca')->get();
        return view('adviser.index',['user'=>Auth::user()->name, 'advisers'=>$adviser])->with('demos',$demos); 
        }else if (Auth::user()->user_role === 'doradca'){
            return redirect('demos/index');
        }
    }
        }

    
     public function edit($id){
         
    $demos = Demo::pluck('name','id');
    
    $adviser = User::findOrFail($id);    
         
    return view('adviser.edit',['demos'=>$demos, 'user'=>Auth::user()->id])->with('adviser',$adviser); ;   
     }
         

     /**
     * [[Method update item from collection clockc]]
     * @return [[View]] [[Description]]
     */
    
     public function update($id, Request $request){
       
       $demo = Demo::findOrFail($request->demo); 

       $clock = Clock::where('demo','=',$demo->name)->first();
       $footer = Footer::where('demo','=',$demo->name)->first();     
  
        $data['user_id'] = $demo->user_id=$request->user_id;
        $data['user_id'] = $clock->user_id=$request->user_id;
        $data['user_id'] = $footer->user_id=$request->user_id; 
        
        $clock->update();     
        $footer->update(); 
         
        if($demo->update()){
        return redirect('adviser');  
        } 
         
        
         }
    
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    $user = User::find($id);
        
    if(!empty($user)){
        $user->delete();
    }    
     Session::flash('message_del', 'Suksesywnie usunełeś Klienta!');   
     return redirect('adviser');    
    }
    
    
    
}


