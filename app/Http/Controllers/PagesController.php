<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CrtPageRequest;
//use Mail;
//use PHPMailer;
use Recaptcha;


class PagesController extends Controller
{
    public function index(){
       $header="To jest nagłowek strony goscia";
    return view('pages.index')->with('header',$header);  
    }
    
    public function contact(){
        $header="To jest nagłowek z szablonu Kontakt";
       
        return view('pages.contact')->with('header',$header);
    }
    
    public function about(){
        $header="To jest nagłowek z szablonu O Nas";
        return view('pages.about')->with('header',$header);
        }
    
    public function store(CrtPageRequest $request){
    $user =  $request->get('name');
    $email = $request->get('email'); 
    $messages = $request->get('description');
     
    
     
        
    if(!empty($request->get('g-recaptcha-response'))){
     if(!empty($user) && !empty($email) && !empty($messages)){
//    Mail::send('emails.reminder', ['user' => $user,'messages'=>$messages], function ($m) use ($user,$email) {
//            $m->from($email, 'CMS Demo');
//
//            $m->to('romanjedras23@gmail.com', $user)->subject('Twoja wiadomość');
//        });       
//        
//        } 
         if($messages !== ''){
         
        $od  = "From: {$email} \r\n";
        $od .= 'MIME-Version: 1.0'."\r\n";
        $od .= 'Content-type: text/html; charset=iso-8859-2'."\r\n"; 
        $adres = "romanjedras23@gmail.com";
        $tytul = "Twoja wiadomość";
        $wiadomosc = "<html><head></head><body>
           <b>Witam serdecznie!</b><br/><p style='color:red'>
           {$messages}</p></body></html>";

        // użycie funkcji mail
   $result = mail($adres, $tytul, $wiadomosc,$od);
           
        if(!$result):?>
        <br/><br/>
        <div class="alert alert-success"><p>
        <?php echo "Error";?>
        </p></div>     
        <?php else:?>
        <div class="alert alert-success"><p>
        <?php echo "Success";?>
      </p></div>   
    <?php     
    endif;
         }
         } 
       unset($user); 
       unset($messages);    
       $user = '';    
       $messages = '';
           
        $header="To jest nagłowek z szablonu Kontakt"; 
       return redirect('/');
    }
    }
    
    
    public function filemanager(){
        $header="To jest nagłowek z szablonu Kontakt"; 
        return view('pages.filemanager');
    }
    
    public function validateCaptchaIsRequired(){
        
    }
    
}
