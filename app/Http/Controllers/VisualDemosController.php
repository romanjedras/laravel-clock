<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VisualDemo;
use App\Http\Requests;
use App\Http\Requests\CrtVisualDemosRequest;
use Auth;
use App\User;
use App\Demo;
use Session;

class VisualDemosController extends Controller
{
    /** Konstruktor włacza środowisko ochronne (blokade dostepu dla osob nie zalogowanych) **/   
      public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    /**
     * [[Method display all collection visualdemo]]
     * @return [[View]] [[Description]]
     */
    
    public function index(){
        
      if(Auth::user()->user_role === 'super_admin' || Auth::user()->user_role === 'doradca'){
          
     $visual_demos = VisualDemo::latest()->get();
   return view('visualDemos.index',['user'=>Auth::user()])->with('visual_demos',$visual_demos);  
        }else{
     return view('customer')->with('id',Auth::user());    
        }  
        
    }
    
    
/** Metoda wyświetla formularz dodaj nowa wisual  **/ 
   
public function create(){
   $users = User::latest()->get();
    $customers = User::where('user_role','=','customer')->get();
    $demos = Demo::pluck('name','name');
    
     return view('visualDemos.create',['demos'=>$demos,'customers'=>$customers,'user_id'=>Auth::user()->id])->with('users',$users);  
}    
    
/** Metoda zapisuje dane nowej vizual.. do kolekcji  **/       
public function store(CrtVisualDemosRequest $request){
    
    $visualDemo = new VisualDemo(array_map("trim", array_map("strip_tags", $request->all()))); 
    Auth::user()->visualdemos()->save($visualDemo); 
    Session::flash('visualdemos_created','Wizualizacja została dodana do systemu');
    
 return redirect('visual'); 
} 
 
 /** Metoda prezentuje formularz edycji  **/       
public function edit($id){  
    
 $visual_demos = VisualDemo::findOrFail($id);
 $users = User::where('user_role','=','customer')->get();
 $demos = Demo::pluck('name','name');   
    
  if(!empty($visual_demos)){
return view('visualDemos.edit',['users'=>$users,'demos'=>$demos,'user_id'=>Auth::user()->id])->with('visual_demos',$visual_demos);
  }else{
  return redirect('visual');       
  }
}
    
/** Metoda aktualizuje dane wskazanej wizualizacji przez id vizual  **/     
 public function update($id, CrtVisualDemosRequest $request){
    if(!empty($id)){
    $visual_demos = VisualDemo::findOrFail($id); 
    $visual_demos->update(array_map("trim", array_map("strip_tags", $request->all())));
    Session::flash('visualdemos_update','Wizualizacja została z aktualizowana');    
    return redirect('visual');     
    }else{
    return view('demos');    
    }

    }
    
}
