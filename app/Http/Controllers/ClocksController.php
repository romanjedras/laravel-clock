<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CrtClockRequest;
use App\Clock;
use App\Demo;
use App\User;
use Auth;
use File;
use App\Footer;
use Session;


class ClocksController extends Controller
{
   /** Konstruktor włacza środowisko ochronne (blokade dostepu dla osob nie zalogowanych) **/   
      public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    /**
     * [[Method display all collection clockc]]
     * @return [[View]] [[Description]]
     */
    public function index(){
        
    if(Auth::user()->user_role === 'super_admin' || Auth::user()->user_role === 'doradca'){
    
     $users = User::latest()->get();   
        
    if(Auth::user()->user_role === 'doradca'){
    $user = User::findOrFail(Auth::user()->id);  
    $cloks = Clock::where('user_id','=',$user->id)->get();      
    }else if(Auth::user()->user_role === 'super_admin'){
     $cloks = Clock::latest()->get();
    }
    return view('clocks.index',['users'=>$users])->with('cloks',$cloks);    
    }else{
     return view('customer')->with('id',Auth::user());    
        }
        
    }
    
     /**
     *[[Method create new item collection clockc]]
     * @return [[View]] [[Description]]
    **/
    
     public function create(){
         
        $demos = Demo::pluck('name','name'); 
         
        return view('clocks.create',['demos'=>$demos])->with('user_id',Auth::user()->id);  
         }
    /**
     * [[Method save new item to collection clockc]]
     * @return [[View]] [[Description]]
     */
    
    public function store(CrtClockRequest $request){
     
//       Clock::create($request->all());
    $clock = new Clock(array_map("trim", array_map("strip_tags", $request->all())));
    Auth::user()->clocks()->save($clock); 
    Session::flash('clock_created','Zegar został dodany do systemu'); 
           
      return redirect('footers/create');
        }
    
    /**
     * [[Method display for to update item from collection clockc]]
     * @return [[View]] [[Description]]
     */
    
    public function edit($id){
        
        
     if(Auth::user()->user_role === 'super_admin' || Auth::user()->user_role === 'doradca'){
   
     $clock = Clock::findOrFail($id);
     $demos = Demo::pluck('name','name');      
       if(!empty($clock)){
    return view('clocks.edit',['demos'=>$demos,'user_id'=>Auth::user()->id])->with('clock',$clock);    
       }else{
        return redirect('demos');   
       }     
         }else{
     return view('customer')->with('id',Auth::user());    
        }    
    }
    
     /**
     * [[Method update item from collection clockc]]
     * @return [[View]] [[Description]]
     */
    
     public function update($id, CrtClockRequest $request){
         
        $item_array = $request->all();
        $clock = Clock::findOrFail($id); 
         
         if(!empty($item_array['demo'])){
            $clock->update($request->all());
         }
         
        return redirect('clocks');;
    }
     /**
     * [[Method show once item from collection clockc]]
     * @return [[View]] [[Description]]
     */
    
    public function show($id){
        
    $clock = Clock::where('demo','=',$id)->first();
    $demos = Demo::pluck('name','name');    
       if(!empty($clock)){
         return view('clocks.edit2',['demos'=>$demos,'user_id'=>Auth::user()->id])->with('clock',$clock);
         }else{
        return redirect('clocks');     
         }}
}
