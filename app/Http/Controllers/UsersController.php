<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUserRequest;
use App\User;
//use Mail;
//use PHPMailer;

class UsersController extends Controller
{
 
     public function index(){
        $users = User::latest()->get();
    return view('users.index')->with('users',$users);  
    }
    
    
    public function edit($id){
      $user = User::findOrFail($id); 
    return view('users.edit')->with('user',$user);      
        
    }
    
      public function show($id){
    $users = User::findOrFail($id);
    $user = User::where('id','=',$users->id)->first();      
    return view('users.edit',[])->with('user',$user); 
 }
    
    
    
    public function create(){
   $users = User::latest()->get();
    return view('users.create',['user_id'=>Auth::user()->id])->with('users',$users);  
}
 
    
    public function update($id, Request $request){
       $user = User::findOrFail($id);
        
        $data['name'] = $user->name=$request->name; 
        $data['email'] = $user->email=$request->email;
        $data['user_role'] = $user->user_role;
        $user->password =bcrypt($request->password);
        $user->remember_token = str_random(100);
        if($user->update()){
        return redirect('users');  
        }
        
        return 1;
        
    }
    
    
}

