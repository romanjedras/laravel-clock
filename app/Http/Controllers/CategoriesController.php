<?php

namespace App\Http\Controllers;
use App\Category;
use App\Http\Requests\CrtCategoriesRequest;
use Auth;
use Session;

//use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * [[Method return all collection from categories]]
     */
    public function index(){
      
       $categories = Category::latest()->get();  
        
      return view('categories.index')->with('categories',$categories);
    }
    
  
    /**
     * [[Method return all collection from categories]]
     */
    public function create(){
      
        return view('categories.create')->with('user_id',Auth::user()->id);
    }
    
     /**
     * [[Method save new item to collection categories]]
     * @return [[View]] [[Description]]
     */
     public function store(CrtCategoriesRequest $request){
     
//       Clock::create($request->all());
    $category = new Category(array_map("trim", array_map("strip_tags", $request->all())));
    Auth::user()->categories()->save($category); 
    Session::flash('categories_created','Kategoria została dodana do systemu'); 
           
      return redirect('categories');
        }
    
    
    
    
    /**
     * [[Method return form edit choose item collection from categories]]
     */
    public function edit($id){
    $category = Category::findOrFail($id); 
    if(!empty($category)){    
     return view('categories.edit',['user_id'=>Auth::user()->id])->with('category',$category);;   
    }else{
    return view('categories');     
     }
        
    }
     /**
     * [[Method return update item collection from categories]]
     */
     public function update($id, CrtCategoriesRequest $request){
       
        $category = Category::findOrFail($id);
        $category->update($request->all());
        
       return redirect('categories');; 
     }
    
    
    
    
}
