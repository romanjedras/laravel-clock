<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateDemoRequest;
use App\Demo;
use App\User;
use App\Category;
use App\VisualDemo;
use File;
use App\Clock;
use App\Footer;
use Auth;
use Session;



class DemosController extends Controller
{
    /** Konstruktor włacza środowisko ochronne (blokade dostepu dla osob nie zalogowanych) **/ 
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    
    /** Metoda przekazuje dane do widoku -> index **/

    public function index(){
       
    if(Auth::user()->user_role === 'super_admin' || Auth::user()->user_role === 'doradca'){
        
        if(Auth::user()->user_role === 'doradca'){
        $user = User::findOrFail(Auth::user()->id); 
        $demos = Demo::where('user_id','=',$user->id)->get();  
        return view('demos.index',['user'=>Auth::user()->name])->with('demos',$demos);       
        
        }else if(Auth::user()->user_role === 'super_admin'){
         $demos = Demo::latest()->get();
       return view('demos.index',['user'=>Auth::user()->name])->with('demos',$demos);        
        }
        }else{
        
        if(Auth::user()->user_role === 'customer'){
//         $id = Auth::user()->id;    
//     
//    $visual_demos = VisualDemo::find($id); 
            
    $visuals_demos = VisualDemo::where('customer_id','=',Auth::user()->name)->get();         
            
    foreach($visuals_demos as $visual_demos){}
            
    if(empty($visual_demos)){  
    return view('demos.home')->with('user',Auth::user());
    }else{
     if(!empty($visual_demos)){
    if(!empty($visual_demos->demo)){
    $times = Clock::where('demo','=',$visual_demos->demo)->first();
    $footers = Footer::where('demo','=',$visual_demos->demo)->first();
    $demos = Demo::where('name','=',$visual_demos->demo)->first(); 
    $dirpath=$_SERVER['DOCUMENT_ROOT']."/filemanager/userfiles/demo/";
    $dirpath.=$demos->name.'/';      
}
        
  if(!empty($demos) && !empty($times) && !empty($footers)){
return  view('customers.show',['times'=>$times,'stopka'=>$footers,'demo'=>$dirpath,'opis'=>$demos->description,'dname'=>$demos->name])->with('demos',$demos);     
        }else{
       Session::flash('demo_request','Demo nie posiada wymaganych składników'); 
    return redirect('customer');    
    }         
        
    }else{
    return redirect('customer');    
    }
    }  
    }
        
    }   
    return 1;    
    }

 /**Metoda wyswietlajaca cała kolekcje**/    

    public function getall(){
    $demos = Demo::latest()->get();
    $cloks = Clock::latest()->get();
    $footers = Footer::latest()->get();
    return view('demos.demo',['cloks'=>$cloks,'footers'=>$footers])->with('demos',$demos);   
    }
    
     /** Metoda wyświetla formularz dodaj nowe demo  **/ 
   
public function create(){
   $users = User::latest()->get();
    
    $categories = Category::pluck('name','id');
    
     return view('demos.create',['categories'=>$categories, 'user_id'=>Auth::user()->id])->with('users',$users);  
}    
    
  /** Metoda zapisuje dane nowego demo do kolekcji  **/    
    
public function store(CreateDemoRequest $request){
    
//Demo::create(array_map("trim", array_map("strip_tags", $request->all())));
    $demo = new Demo(array_map("trim", array_map("strip_tags", $request->all())));
    Auth::user()->demos()->save($demo);    
   
//    $categoryIds = $request->input('CategoryList');
//    $demo->categories()->attach($categoryIds);
    
    Session::flash('demo_created','Demo zostało dodane do systemu');
    $data = $request->all();
    $path = $data['name'];
    $dirpath=$_SERVER['DOCUMENT_ROOT']."/filemanager/userfiles/demo/";
    $dirpath.=$path;
    $mode=0777;
    
    if(!is_dir($dirpath)){
     File::MakeDirectory($dirpath,$mode,true);     
    }
    
   return redirect('clocks/create');
   }
    
    /** Metoda prezentuje formularz edycji  **/   
    
    public function edit($id){
        
    if(Auth::user()->user_role === 'super_admin' || Auth::user()->user_role === 'doradca'){
         $demo = Demo::findOrFail($id);  
        $categories = Category::pluck('name','id');
        
    $path = $demo->name;
    $dirpath=$_SERVER['DOCUMENT_ROOT']."/filemanager/userfiles/demo/";
    $dirpath.=$path;
    $mode=0777;
    
    if(!is_dir($dirpath)){
     File::MakeDirectory($dirpath,$mode,true);     
    }    
        
        
    if(!empty($demo)){
    return view('demos.edit',['categories'=>$categories,'user_id'=>Auth::user()->id])->with('demo',$demo);      
    }else{
    return redirect('demos');        
        }
        }else{
     return view('customer')->with('id',Auth::user());    
        }        
        } 
    
     /** Metoda wskazuje, do której kategorii demo należy  **/   
    public function show3($id){
    $demo = Demo::findOrFail($id);
    $footers = Footer::where('demo','=',$demo->name)->first();
        
   if(NULL == $footers){
        return redirect('demos');  
      }else{
        if(!empty($demo) && !empty($demo->name)){
        return view('demos.categories')->with('category',$footers->model);     
        }else{
          return redirect('demos');   
        }      
      }  
    }
    
    /** Metoda aktualizuje dane wskazanego demo przez id demo  **/   
    
    public function update($id, CreateDemoRequest $request){
        $demo = Demo::findOrFail($id); 
        $demo->update(array_map("trim", array_map("strip_tags", $request->all())));
     
        if(!empty($id)){
        $categoryIds = $request->input('CategoryList');
        $categoryIds=['categoryIds'=>$categoryIds];    
        $demo->categories()->sync($categoryIds);    
        }
        
      return redirect('demos');
        
    }
    
    /** Metoda prezentuje cała kolekcje dem  **/  
   
      public function show($id){
       $demo = Demo::findOrFail($id);
    
   
          
    if(!empty($demo->name)){
    $times = Clock::where('demo','=',$demo->name)->first();
    $footers = Footer::where('demo','=',$demo->name)->first();
    $users = User::where('id','=',$demo->user_id)->first();    
    }      
           
    if(!empty($demo) && !empty($times) && !empty($footers)){
        return  view('demos.show',['times'=>$times,'footer'=>$footers,'auth'=>$users])->with('demo',$demo); ;     
          }else{
       Session::flash('demo_request','Demo nie posiada wymaganych składników'); 
        return redirect('demos');    
         }
        }
    
    
    
    
    
public function demos2(){
    
    return view('demos.home')->with('id',Auth::user()->id);
    }

    /**Metoda przekazujaca dane do wizualizacji na frontcie**/
    
public function show2($id){
    
$times = Clock::where('demo','=',$id)->first();
$footers = Footer::where('demo','=',$id)->first();
$demo = Demo::where('name','=',$id)->first();  
    
//echo "<pre>"; print_r($demo->description); echo "</pre>";    
    
$users = User::where('id','=',$demo->user_id)->first();     
  
$dirpath="../filemanager/userfiles/demo/";
$dirpath.= $demo->name.'/'; 
    if(!empty($times) && !empty($footers) && !empty($demo) && !empty($users)){

    $dirpath=$_SERVER['DOCUMENT_ROOT']."/filemanager/userfiles/demo/";
    $dirpath.= $demo->name.'/'; 
      
    if ($this->is_dir_empty($dirpath)) {
        return redirect('demos'); 
    }else{
     return view('demos.visualization',['stopka'=>$footers,'demo'=>$dirpath,'dname'=>$demo->name,'opis'=>$demo->description, 'users'=>$users])->with('times',$times);     
    }
      }
    }
    
/**Metoda logout jak sama nazwa wskazuje słuzy do wylogowania**/   
    public function logout(){
        return view('auth.login');
    }
    
public function is_dir_empty($dir) {
  if (!is_readable($dir)) return NULL; 
  $handle = opendir($dir);
  while (false !== ($entry = readdir($handle))) {
    if ($entry != "." && $entry != "..") {
      return FALSE;
    }
  }
  return TRUE;
}
    
    }
