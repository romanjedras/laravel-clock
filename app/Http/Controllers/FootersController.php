<?php

namespace App\Http\Controllers;



use Illuminate\Http\Request;
use App\Http\Requests\CrtFooterRequest;
use App\Footer;
use App\Demo;
use App\User;
use App\Category;
use Auth;
use Session;

class FootersController extends Controller
{
   /** Konstruktor włacza środowisko ochronne (blokade dostepu dla osob nie zalogowanych) **/  
      public function __construct()
    {
        $this->middleware('auth');
    }
    
    //
     /**
     * [[Method display all collection footers]]
     * @return [[View]] [[Description]]
     */
    
   public function index(){
       
       $users = User::latest()->get();    
       
       if(Auth::user()->user_role === 'doradca'){
        $user = User::findOrFail(Auth::user()->id); 
        $footers = Footer::where('user_id','=',$user->id)->get();  
        }else if(Auth::user()->user_role === 'super_admin'){
        $footers = Footer::latest()->get();
        }
      return view('footers.index',['users'=>$users])->with('footers',$footers);  
       }
    
     /**
     * [[Method create new item to collection footers]]
     * @return [[View]] [[Description]]
     */
    
    
     public function create(){
        $categories = Category::pluck('name','name');
        $demos = Demo::pluck('name','name'); 
        return view('footers.create',['demos'=>$demos,'categories'=>$categories])->with('user_id',Auth::user()->id);
    }
    
      /**
     * [[Method save new item to collection footers]]
     * @return [[View]] [[Description]]
     */
    
    
    public function store(CrtFooterRequest $request){
        
//        Footer::create($request->all());
        
    $footer = new Footer(array_map("trim", array_map("strip_tags", $request->all())));
    Auth::user()->footers()->save($footer); 
    Session::flash('footers_created','Stopka została dodana do systemu');
        
        
        return redirect('demos/');
    }
   
    /**
     * [[Method edit display form to edit data item in collection footers]]
     * @return [[View]] [[Description]]
     */
    
    public function edit($id){
        
        $footer = Footer::findOrFail($id);
       $demos = Demo::pluck('name','name'); 
       return view('footers.edit',['demos'=>$demos,'user_id'=>Auth::user()->id])->with('footer',$footer);
    }
    
    /**
     * [[Method update choose item in collection footers]]
     * @return [[View]] [[Description]]
     */
    
    public function update($id, CrtFooterRequest $request){
        
       $footer = Footer::findOrFail($id);
        $footer->update($request->all());
        
        return redirect('footers');
    }
    
   /**
     * [[Method show choose item from collection footers]]
     * @return [[View]] [[Description]]
     */ 
    
    public function show($id){
        $footer = Footer::where('demo','=',$id)->first();
        $categories = Category::pluck('name','name');
         if(!empty($footer)){
         return view('footers.edit2',['user_id'=>Auth::user()->id, 'categories'=>$categories])->with('footer',$footer);     
         }else{
           return redirect('footers');
         } 
        
    }
}
