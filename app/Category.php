<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
    protected $fillable = ['name'];
    
    /**Kategoria jest przypisana do wielu demo**/
    
    public function demos(){
        return $this->belongsToMany('App\Demo')->withTimestamps();
    } 
    
   public function user(){
        return $this->belongsTo('App\User');   
    }
    
    
    
}
