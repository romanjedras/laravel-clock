<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VisualDemo extends Model
{
    protected $fillable = ['user_id','customer_id','title','url','description','demo'];
    
    
    public function user(){
        
     return $this->belongsTo('App\User');   
    }
    
    
    public function demos(){
        return $this->belongsTo('App\Demo'); 
    }
    
   
}
