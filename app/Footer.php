<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footer extends Model
{
    protected $fillable = ['user_id','date','nazwa','model','cena','demo'];
    
    
    
     public function user(){
     return $this->belongsTo('App\User');   
    }
    
    
    
    /** Każde demo może należeć do wybranej kategorii**/
    public function categories(){
        return $this->belongsToMany('App\Category')->withTimestamps();
    }
    
    /** Lista Id kategorii dla jednego demo**/
    public function getCategoryListAttribute(){
        return $this->categories->pluck('id')->all();
    }
}
