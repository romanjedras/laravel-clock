<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','PagesController@index');


  

Route::group(['middleware'=>['web']],function(){
Route::get('contact', "PagesController@contact");
Route::post('contact', "PagesController@store");    
Route::get('filemanager', "PagesController@filemanager");    
    
Route::get('about', "PagesController@about"); 
Route::get('filemanager', "PagesController@filemanager");    
    
Route::resource('demos','DemosController');  
Route::resource('clocks','ClocksController');
Route::resource('footers','FootersController');
Route::resource('categories', "CategoriesController");  
Route::resource('customers','CustomersController'); 
Route::resource('visual','VisualDemosController');
Route::resource('users','UsersController');  
Route::resource('adviser','AdviserController');     
    
Route::get('clocks/{name}','ClocksController@show');
Route::get('footers/{name}','FootersController@show');
Route::get('demos2/{name}', 'DemosController@show2');  
Route::get('category/{name}', 'DemosController@show3');      
    

    
    
    
Auth::routes();       
Route::get('/logout', 'DemosController@logout');
Route::get('/home', 'HomeController@index');

    
Route::post('store2', 'DemosController@store');    
Route::post('store_clock', 'ClocksController@store');     
Route::post('store', 'FootersController@store'); 
Route::post('store_customer', 'CustomersController@store');
Route::post('store_category', 'CategoriesController@store');
Route::post('store_visual', 'VisualDemosController@store');  
    
    
});

